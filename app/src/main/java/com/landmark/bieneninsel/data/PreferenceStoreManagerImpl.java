package com.landmark.bieneninsel.data;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.landmark.bieneninsel.model.PlantCell;

public class PreferenceStoreManagerImpl implements PreferenceStoreManager {

    final String LEVEL_ONE_FIRST_GROUP_COUNTER = "LevelOneFirstGroupCounter";
    final String LEVEL_ONE_SECOND_GROUP_COUNTER = "LevelOneSecondGroupCounter";
    final String LEVEL_ONE_THIRD_GROUP_COUNTER = "LevelOneThirdGroupCounter";
    final String LOCKING_CELL = "LockingCell";

    private final GameData preferences;
    private final Gson gsonObj;

    public PreferenceStoreManagerImpl(GameData preferences) {
        this.preferences = preferences;
        this.gsonObj = new Gson();
    }

    @Override
    public void setHoneycombData(String plantId, PlantCell plantCell) {
        preferences.putString(plantId, serializeObject(plantCell));
    }

    @Override
    public PlantCell getHoneycombData(String plantId) {
        String plantCellString = preferences.getString(plantId, null);
        return deserializeObject(plantCellString, new TypeToken<PlantCell>() {
        }.getType());
    }

    @Override
    public int getLevelOneFirstGroupCounter() {
        return preferences.getInt(LEVEL_ONE_FIRST_GROUP_COUNTER, 0);
    }

    @Override
    public void setLevelOneFirstGroupCounter(int counter) {
        preferences.putInt(LEVEL_ONE_FIRST_GROUP_COUNTER, counter);
    }

    @Override
    public int getLevelOneSecondGroupCounter() {
        return preferences.getInt(LEVEL_ONE_SECOND_GROUP_COUNTER, 0);
    }

    @Override
    public void setLevelOneSecondGroupCounter(int counter) {
        preferences.putInt(LEVEL_ONE_SECOND_GROUP_COUNTER, counter);
    }


    @Override
    public int getLevelOneThirdGroupCounter() {
        return preferences.getInt(LEVEL_ONE_THIRD_GROUP_COUNTER, 0);
    }

    @Override
    public void setLevelOneThirdGroupCounter(int counter) {
        preferences.putInt(LEVEL_ONE_THIRD_GROUP_COUNTER, counter);
    }

    @Override
    public void setLockingCell(PlantCell plantCell) {
        preferences.putString(LOCKING_CELL, serializeObject(plantCell));
    }

    @Override
    public PlantCell getLockingCell() {
        String plantCellString = preferences.getString(LOCKING_CELL, null);
        return deserializeObject(plantCellString, new TypeToken<PlantCell>() {
        }.getType());
    }

    @Override
    public void setPoint(String type, int point) {
        preferences.putInt(type, point);
    }

    @Override
    public int getPoint(String type) {
        return preferences.getInt(type, 0);
    }

    private String serializeObject(Object object) {
        return gsonObj.toJson(object);
    }

    private <T> T deserializeObject(String value, Type typeOf) {
        if (value == null) {
            return null;
        }
        try {
            return gsonObj.fromJson(value, typeOf);
        } catch (Throwable throwable) {
            return null;
        }
    }

    @Override
    public void resetAll() {
        this.preferences.clearData();
    }
}
