package com.landmark.bieneninsel.data;

public interface Constants {

    // global
    int GRID_RADIUS = 5; // grid radius - the count of rings around the central node.
    int LEVEL_ONE_FIRST_GROUP_COUNTER = 7; // first selected 7 flowers on first group
    int LEVEL_ONE_SECOND_GROUP_COUNTER = 7; // second selected 7 flowers on second group
    int LEVEL_ONE_THIRD_GROUP_COUNTER = 5; // last selected 5 flowers on third group
    int DAY_TIME_START = 18; // bee: new day starts at 9am
    int DAY_TIME_END = 20; // bee: current day ends at 5pm

    // time-slots for bees
    int RANDOM_TIME_TO_FLY_BEE = 1800000; // bee: fly for a random period between 1-30 minutes
    int RANDOM_TIME_TO_CHOOSE_BEE = 1800000; // bee: choose a random time from next 30 minutes and call a random bee to enter Bieneninsel
    int RANDOM_TIME_TO_CALL_BEE_TO_VISIT_FLOWER = 1800000; // bee: choose a random time from next 30 minutes and call a random bee to visit a random marked happy (ready) flower

    int TIME_TO_CHANGE_DIRECTION = 10000; // bee: change fly destination every 10 sec
    int TIME_TO_FLY_OUT = 10000; // bee: fly out of board for 10 sec
    int TIME_TO_LAND_ON_FLOWER = 10000; // bee: land on marked flower for 10 sec
    int TIME_TO_STAY_ON_FLOWER = 30000; // bee: stay on flower for 30 sec

    // time-slots for flowers
    int GROUP_ONE = 1; // first group id and grow happy flower next day rule
    int GROUP_TWO = 2; // second group id and grow plant next day, happy flower after 2 days rule
    int GROUP_THREE = 3; // third group id and grow plant next day, happy flower after 3 days rule

    int TIME_TO_WATER = 20000; // flower: throw water for 20 sec
    int TIME_TO_UNMARK_FLOWER = 2; // flower: unmark flower after 2 hours from last mark
    int DAY_TO_CHECK_FLOWER = 1; // flower: resets a flower state after being happy or sad after 1 day (calendar day)
    int FLOWER_ANIMATION_DURATION = 3000; // flower: animate flower for a duration of 3 sec

}
