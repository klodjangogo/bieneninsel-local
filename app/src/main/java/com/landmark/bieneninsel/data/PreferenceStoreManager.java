package com.landmark.bieneninsel.data;

import com.landmark.bieneninsel.model.PlantCell;

public interface PreferenceStoreManager {

    void setHoneycombData(String plantId, PlantCell plantCell);

    PlantCell getHoneycombData(String plantId);

    int getLevelOneFirstGroupCounter();

    void setLevelOneFirstGroupCounter(int counter);

    int getLevelOneSecondGroupCounter();

    void setLevelOneSecondGroupCounter(int counter);

    int getLevelOneThirdGroupCounter();

    void setLevelOneThirdGroupCounter(int counter);

    void setLockingCell(PlantCell plantCell);

    PlantCell getLockingCell();

    void setPoint(String type, int point);

    int getPoint(String type);

    void resetAll();

}