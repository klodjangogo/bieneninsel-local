package com.landmark.bieneninsel.data;

import java.util.Set;

public interface GamePreferences {

    void putString(String key, String value);

    void putInt(String key, int value);

    void putFloat(String key, float value);

    void putLong(String key, long value);

    void putBoolean(String key, boolean value);

    String getString(String key, String defaultValue);

    int getInt(String key, int defaultValue);

    float getFloat(String key, float defaultValue);

    long getLong(String key, long defaultValue);

    boolean getBoolean(String key, boolean defaultValue);

    void putStringSet(String key, Set<String> value);

    Set<String> getStringSet(String key);

    void remove(String... keys);

    void clearData();

}
