package com.landmark.bieneninsel.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.landmark.bieneninsel.R;

import java.util.Set;

public class GameData implements GamePreferences {

    private static GameData sInstance = null;

    private SharedPreferences preferences;

    private SharedPreferences.Editor editor;

    public final synchronized static void initialize(Context context) {
        if (sInstance == null) {
            sInstance = new GameData(context);
        }
    }

    public final synchronized static GameData getInstance() throws NullPointerException {
        if (sInstance == null) {
            throw new NullPointerException("Not initialized " +
                    "Call initialize in your GameApplication.onCreate() to create a new instance");
        }
        return sInstance;
    }

    protected GameData(Context context) {
        preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @Override
    public void putString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    @Override
    public void putInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    @Override
    public void putFloat(String key, float value) {
        editor.putFloat(key, value);
        editor.apply();
    }

    @Override
    public void putLong(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    @Override
    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    @Override
    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    @Override
    public int getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }

    @Override
    public float getFloat(String key, float defaultValue) {
        return preferences.getFloat(key, defaultValue);
    }

    @Override
    public long getLong(String key, long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    @Override
    public void putStringSet(String key, Set<String> value) {
        editor.putStringSet(key, value);
        editor.apply();
    }

    @Override
    public Set<String> getStringSet(String key) {
        return preferences.getStringSet(key, null);
    }

    @Override
    public void remove(String... keys) {
        for (String key : keys) {
            editor.remove(key);
        }
        editor.apply();
    }

    @Override
    public void clearData() {
        editor.clear().commit();
        editor.apply();
    }

}

