package com.landmark.bieneninsel;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.landmark.bieneninsel.data.Constants;
import com.landmark.bieneninsel.data.GameData;
import com.landmark.bieneninsel.data.PreferenceStoreManager;
import com.landmark.bieneninsel.data.PreferenceStoreManagerImpl;
import com.landmark.bieneninsel.events.DayEndedBroadcastReceiver;
import com.landmark.bieneninsel.events.DayStartedBroadcastReceiver;
import com.landmark.bieneninsel.events.FlyBeeTask;
import com.landmark.bieneninsel.events.TikTokReceiver;
import com.landmark.bieneninsel.events.VisitFlowerTask;
import com.landmark.bieneninsel.model.Cube;
import com.landmark.bieneninsel.model.GameBoard;
import com.landmark.bieneninsel.model.Grid;
import com.landmark.bieneninsel.model.Hex;
import com.landmark.bieneninsel.model.InvalidBoardException;
import com.landmark.bieneninsel.model.LevelOne;
import com.landmark.bieneninsel.model.PairID;
import com.landmark.bieneninsel.model.PlantCell;
import com.landmark.bieneninsel.model.ScreenSize;
import com.landmark.bieneninsel.model.StorageMap;
import com.landmark.bieneninsel.ui.HexagonImageView;
import com.landmark.bieneninsel.ui.curencies.CurrencyView;
import com.landmark.bieneninsel.ui.listener.HexFieldTouchListener;
import com.landmark.bieneninsel.usecases.BeeUseCase;
import com.landmark.bieneninsel.usecases.CurrenciesUseCase;
import com.landmark.bieneninsel.usecases.FlowerUseCase;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    private PreferenceStoreManager preferenceStoreManager;
    private RelativeLayout mRelativeLayout;
    private View exitDoorView;
    private Grid grid;
    private HexFieldTouchListener hexFieldTouchListener;
    private LevelOne levelOneData;
    private List<View> hexFieldViews;
    private Map<String, LottieAnimationView> hexFieldViewAnimations;
    private AlarmManager alarmManager;
    private Intent tikTokIntent;
    private FlowerUseCase flowerUseCase;
    private BeeUseCase beeUseCase;
    private CurrenciesUseCase currenciesUseCase;
    private Timer flyBeeDailyTimer;
    private List<LottieAnimationView> beeViews;
    private List<View> pivotViews;
    private FlyBeeTask flyBeeTask;
    private Timer visitFlowerDailyTimer;
    private VisitFlowerTask visitFlowerTask;
    private CurrencyView blutenstaubView;
    private CurrencyView pflegepunkteView;

    private DayStartedBroadcastReceiver dayStartedBroadcastReceiver = new DayStartedBroadcastReceiver() {
        @Override
        public void onDayStarted() {
            flyBeeTask.run();
            visitFlowerTask.run();
        }
    };

    private DayEndedBroadcastReceiver dayEndedBroadcastReceiver = new DayEndedBroadcastReceiver() {
        @Override
        public void onDayEnded() {
            flyBeeDailyTimer.cancel();
            visitFlowerDailyTimer.cancel();
            beeUseCase.exitBees();
        }
    };

    private BroadcastReceiver updateFlowerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Integer flowerIdFirst = (Integer) intent.getIntExtra("FLOWER_ID_FIRST", -1000);
            Integer flowerIdSecond = (Integer) intent.getIntExtra("FLOWER_ID_SECOND", -1000);
            flowerUseCase.updateHexField(new PairID(flowerIdFirst, flowerIdSecond));
        }
    };

    private BroadcastReceiver flowerAlarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PairID flowerId;
            switch (intent.getAction()) {
                case "ALARM_TO_GROW_FLOWER":
                    LocalDateTime timeToGrowFlower = (LocalDateTime) intent.getSerializableExtra("TIME_TO_GROW_FLOWER");
                    flowerId = (PairID) intent.getSerializableExtra("FLOWER_TO_GROW");
                    setNextAlarm(timeToGrowFlower, flowerId);
                    break;
                case "ALARM_TO_CHECK_FLOWER":
                    LocalDateTime timeToCheckFlower = (LocalDateTime) intent.getSerializableExtra("TIME_TO_CHECK_FLOWER");
                    flowerId = (PairID) intent.getSerializableExtra("FLOWER_TO_CHECK");
                    setNextAlarm(timeToCheckFlower, flowerId);
                    break;
                case "ALARM_TO_UNMARK_FLOWER":
                    LocalDateTime timeToUnMarkFlower = (LocalDateTime) intent.getSerializableExtra("TIME_TO_UNMARK_FLOWER");
                    flowerId = (PairID) intent.getSerializableExtra("FLOWER_TO_UNMARK");
                    setNextAlarm(timeToUnMarkFlower, flowerId);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        findViews();
        preferenceStoreManager = new PreferenceStoreManagerImpl(GameData.getInstance());
        currenciesUseCase = new CurrenciesUseCase(this, preferenceStoreManager, blutenstaubView, pflegepunkteView);
        flowerUseCase = new FlowerUseCase(this, preferenceStoreManager, currenciesUseCase);
        beeUseCase = new BeeUseCase(this, flowerUseCase, beeViews, exitDoorView);
        hexFieldTouchListener = new HexFieldTouchListener(this);
        hexFieldViews = new ArrayList<>();
        hexFieldViewAnimations = new HashMap<>();
        setupAlarm();
        initGridView(Constants.GRID_RADIUS);
        flyBeeDailyTimer = new Timer();
        visitFlowerDailyTimer = new Timer();
        flyBeeTask = new FlyBeeTask(MainActivity.this, flyBeeDailyTimer, beeUseCase);
        visitFlowerTask = new VisitFlowerTask(MainActivity.this, visitFlowerDailyTimer, beeUseCase);
        LocalTime now = LocalTime.now();
        LocalTime nineAM = LocalTime.now().withHour(9).withMinute(0);
        LocalTime fivePM = LocalTime.now().withHour(17).withMinute(0);

        // fly bees and visit flowers if app is opened anytime between 9am - 5pm
        if (now.isAfter(nineAM) && now.isBefore(fivePM)) {
            flyBeeTask.run();
            visitFlowerTask.run();
        }

//        // TODO: delete for release
//        if (now.getMinute() > 1) {
//            flyBeeTask.run();
//            visitFlowerTask.run();
//        }
    }

    // executed every time the app comes to foreground and updates the Honeycomb
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            flowerUseCase.restoreHoneycomb();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(updateFlowerReceiver, new IntentFilter("ALARM_TO_UPDATE_FLOWER"));
        registerReceiver(flowerAlarmReceiver, new IntentFilter("ALARM_TO_GROW_FLOWER"));
        registerReceiver(flowerAlarmReceiver, new IntentFilter("ALARM_TO_CHECK_FLOWER"));
        registerReceiver(flowerAlarmReceiver, new IntentFilter("ALARM_TO_UNMARK_FLOWER"));
        registerReceiver(dayStartedBroadcastReceiver, DayStartedBroadcastReceiver.Companion.getIntentFilter());
        registerReceiver(dayEndedBroadcastReceiver, DayEndedBroadcastReceiver.Companion.getIntentFilter());

        if (beeUseCase.getFlyingBees().contains(beeViews.get(0))) beeUseCase.getGameAnimatorBeeOne().getBeeAnimator().start();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(1))) beeUseCase.getGameAnimatorBeeTwo().getBeeAnimator().start();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(2))) beeUseCase.getGameAnimatorBeeThree().getBeeAnimator().start();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(3))) beeUseCase.getGameAnimatorBeeFour().getBeeAnimator().start();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(4))) beeUseCase.getGameAnimatorBeeFive().getBeeAnimator().start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(0))) beeUseCase.getGameAnimatorBeeOne().getBeeAnimator().resume();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(1))) beeUseCase.getGameAnimatorBeeTwo().getBeeAnimator().resume();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(2))) beeUseCase.getGameAnimatorBeeThree().getBeeAnimator().resume();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(3))) beeUseCase.getGameAnimatorBeeFour().getBeeAnimator().resume();
        if (beeUseCase.getFlyingBees().contains(beeViews.get(4))) beeUseCase.getGameAnimatorBeeFive().getBeeAnimator().resume();
    }

    private void findViews() {
        mRelativeLayout = (RelativeLayout) findViewById(R.id.gridLayout);
        beeViews = new ArrayList<LottieAnimationView>() {{
            add(findViewById(R.id.beeOne));
            add(findViewById(R.id.beeTwo));
            add(findViewById(R.id.beeThree));
            add(findViewById(R.id.beeFour));
            add(findViewById(R.id.beeFive));
        }};

        pivotViews = new ArrayList<View>() {{
            add(findViewById(R.id.pointOne));
            add(findViewById(R.id.pointTwo));
            add(findViewById(R.id.pointThree));
            add(findViewById(R.id.pointFour));
            add(findViewById(R.id.pointFive));
            add(findViewById(R.id.pointSix));
            add(findViewById(R.id.pointSeven));
            add(findViewById(R.id.pointEight));
            add(findViewById(R.id.pointNine));
            add(findViewById(R.id.pointTen));
            add(findViewById(R.id.pointEleven));
            add(findViewById(R.id.pointTwelve));
            add(findViewById(R.id.pointThirteen));
        }};

        exitDoorView = findViewById(R.id.exitDoor);
        blutenstaubView = findViewById(R.id.blutenstaub);
        pflegepunkteView = findViewById(R.id.pflegepunkte);
    }

    private void setupAlarm() {
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        tikTokIntent = new Intent(this, TikTokReceiver.class);
    }

    private void initGridView(int radius) {
        int scale = setGridDimensions(radius);
        setGridNodes(radius, scale); // init node elements
    }

    private int setGridDimensions(int radius) {
        // Gets the layout params that will allow to resize the layout
        ViewGroup.LayoutParams params = mRelativeLayout.getLayoutParams();
        // TODO: The radius of the single node in grid. This will make it look like 3D.
        mRelativeLayout.setScaleX(0.7f); // flat

        // Get display metrics
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;
        int displayHeight = size.y;

        // If in landscape mode, keep the width small as in portrait mode
        if (displayWidth > displayHeight) displayWidth = displayHeight;

        int horizontalPadding = (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
        displayWidth -= 2 * horizontalPadding;

        // Calculate the scale: the radius of single node.
        // TODO: scales grid. Bigger scale bigger grid.
        int scale;
        boolean tablet = getResources().getBoolean(R.bool.isOver600); //a 10” tablet (720x1280 mdpi, 800x1280 mdpi, etc). or a 7” tablet (600x1024 mdpi).

        TypedValue scaleFactorValue = new TypedValue();
        getResources().getValue(R.dimen.scale_factor, scaleFactorValue, true);
        float scaleFactor = scaleFactorValue.getFloat();

        if (tablet) {
            scale = (int) (displayWidth / ((2.3 * radius + 3)));
        } else {
            scale = (int) (displayWidth / ((scaleFactor * radius + 3)));
        }

        // Changes the height and width of the grid to the specified *pixels*
        params.width = Grid.getGridWidth(radius, scale);
        params.height = Grid.getGridHeight(radius, scale);

        return scale;
    }

    private Grid setGridNodes(int radius, int scale) {
        try {
            int[][] colorNodesArray = setupGameBoard(radius);
            StorageMap storageMap = new StorageMap(radius, colorNodesArray);
            grid = new Grid(radius, scale);
            levelOneData = new LevelOne();

            for (Cube cube : grid.nodes) {
                Hex hex = cube.toHex();
                HexagonImageView hexFieldView = new HexagonImageView(this);
                int hexFieldColor = storageMap.getObjectByCoordinate(hex.getQ(), hex.getR());
                PairID tag = new PairID(hex.getQ(), hex.getR());
                hexFieldView.setTag(tag);

                if (hexFieldColor == 0) {
                    hexFieldView.setOnTouchListener(null);
//                    hexFieldView.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    if (unlockLevelOne(hexFieldView, hexFieldColor)) levelOneData.addPlantCell(hexFieldView, hexFieldColor, hex);
                    // TODO: uncomment unlock Level 2
//                    unlockLevelTwo(hexFieldView, hexFieldColor);
                    // TODO: uncomment to unlock Level 3
//                    unlockLevelThree(hexFieldView, hexFieldColor);
                    hexFieldView.setHex(hex);
                    hexFieldView.setBgColor(hexFieldColor);
                }
                addViewToLayout(hexFieldView, hex, grid, hexFieldColor);
            }
            setupHexFieldTouchListener(levelOneData);
            return grid;
        } catch (InvalidBoardException ibe) {
            Toast.makeText(MainActivity.this, ibe.getMessage(), Toast.LENGTH_LONG).show();
            ibe.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "Sorry, there was a problem initializing the game.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return null;
    }

    // generates a two dimensional array of drawables (hex-fields) or 0s (cutting edges)
    private int[][] setupGameBoard(int radius) throws InvalidBoardException {
        GameBoard gameBoard = new GameBoard();
        if (!gameBoard.isValidRadius(radius)) {
            throw new InvalidBoardException("Game board dimensions are invalid!");
        }
        gameBoard.setRadius(radius);
        return gameBoard.generateGameBoard();
    }

    public boolean unlockLevelOne(View hexFieldView, Integer hexFieldColor) {
        if (hexFieldColor == R.color.groundDark) {
            hexFieldView.setOnTouchListener(hexFieldTouchListener);
            return true;
        }
        return false;
    }

    // TODO
    public void unlockLevelTwo(View hexFieldView, Integer hexFieldColor) {
        if (hexFieldColor == R.color.groundMedium) {
//            hexFieldView.setOnTouchListener(hexFieldTouchListener);
        }
    }

    public void unlockLevelThree(View hexFieldView, Integer hexFieldColor) {
        if (hexFieldColor == R.color.groundLight) {
//            hexFieldView.setOnTouchListener(hexFieldTouchListener);
        }
    }

    private void addViewToLayout(View view, Hex hex, Grid grid, Integer hexFieldColor) {
        // add to view
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(grid.width, grid.height);
        params.addRule(RelativeLayout.RIGHT_OF, R.id.centerLayout);
        params.addRule(RelativeLayout.BELOW, R.id.centerLayout);
        mRelativeLayout.addView(view, params);

        // inflates flower lottie_animation_view to only Level 1 hex-fields
        if (hexFieldColor != null && hexFieldColor == R.color.groundDark) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View animationView = inflater.inflate(R.layout.flower_animation_view, mRelativeLayout, false);
            animationView.setVisibility(View.GONE);
            mRelativeLayout.addView(animationView, params);
            ViewCompat.setTranslationZ(animationView, 1);
            hexFieldViewAnimations.put(view.getTag().toString(), (LottieAnimationView) animationView);
        }

        // set coordinates
        Point p = grid.hexToPixel(hex);
        params.leftMargin = -grid.centerOffsetX + p.x;
        params.topMargin = -grid.centerOffsetY + p.y;

        hexFieldViews.add(view);
    }

    public void setupHexFieldTouchListener(LevelOne levelOneData) {
        hexFieldTouchListener.setGrid(grid);
        List<PlantCell> plantCells = levelOneData.getPlantCells();

        for (int index = 0; index < plantCells.size(); index++) {
            PlantCell plantCell = plantCells.get(index);
            PlantCell savedCell = preferenceStoreManager.getHoneycombData(plantCell.getId().toString());
            if (savedCell != null) {
                levelOneData.getPlantCells().get(index).setId(savedCell.getId());
                levelOneData.getPlantCells().get(index).setCoordinatesId(savedCell.getCoordinatesId());
                levelOneData.getPlantCells().get(index).setColor(savedCell.getColor());
                levelOneData.getPlantCells().get(index).setLevel(savedCell.getLevel());
                levelOneData.getPlantCells().get(index).setGroupId(savedCell.getGroupId());
                levelOneData.getPlantCells().get(index).setSoiled(savedCell.isSoiled());
                levelOneData.getPlantCells().get(index).setFlourished(savedCell.isFlourished());
                levelOneData.getPlantCells().get(index).setWatered(savedCell.isWatered());
                levelOneData.getPlantCells().get(index).setPollinated(savedCell.isPollinated());
                levelOneData.getPlantCells().get(index).setDayToGrowFlower(savedCell.getDayToGrowFlower());
                levelOneData.getPlantCells().get(index).setLastTimeWatered(savedCell.getLastTimeWatered());
                levelOneData.getPlantCells().get(index).setLastTimePollinated(savedCell.getLastTimePollinated());
                levelOneData.getPlantCells().get(index).setHappy(savedCell.isHappy());
                levelOneData.getPlantCells().get(index).setMarked(savedCell.isMarked());
                levelOneData.getPlantCells().get(index).setTimeToUnMarkFlower(savedCell.getTimeToUnMarkFlower());
                levelOneData.getPlantCells().get(index).setBusy(savedCell.isBusy());
            }
        }

        flowerUseCase.setLevelOneData(levelOneData);
        flowerUseCase.setHexFieldViews(hexFieldViews);
        flowerUseCase.setHexFieldViewAnimations(hexFieldViewAnimations);
        flowerUseCase.setHexFieldTouchListener(hexFieldTouchListener);
        beeUseCase.setPivotPoints(pivotViews);
        hexFieldTouchListener.setFlowerUseCase(flowerUseCase);
    }

    public void setNextAlarm(LocalDateTime nextAlarmTime, PairID flowerId) {
        final int id = (int) System.currentTimeMillis();
        tikTokIntent.putExtra("FLOWER_ID_FIRST", flowerId.first);
        tikTokIntent.putExtra("FLOWER_ID_SECOND", flowerId.second);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id, tikTokIntent, PendingIntent.FLAG_ONE_SHOT);

        if (nextAlarmTime != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, nextAlarmTime.getHour());
            calendar.set(Calendar.MINUTE, nextAlarmTime.getMinute());
            calendar.set(Calendar.DAY_OF_MONTH, nextAlarmTime.getDayOfMonth());

            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        beeUseCase.getGameAnimatorBeeOne().getBeeAnimator().pause();
        beeUseCase.getGameAnimatorBeeTwo().getBeeAnimator().pause();
        beeUseCase.getGameAnimatorBeeThree().getBeeAnimator().pause();
        beeUseCase.getGameAnimatorBeeFour().getBeeAnimator().pause();
        beeUseCase.getGameAnimatorBeeFive().getBeeAnimator().pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(updateFlowerReceiver);
        unregisterReceiver(flowerAlarmReceiver);
        unregisterReceiver(dayStartedBroadcastReceiver);
        unregisterReceiver(dayEndedBroadcastReceiver);

        beeUseCase.getGameAnimatorBeeOne().getBeeAnimator().end();
        beeUseCase.getGameAnimatorBeeTwo().getBeeAnimator().end();
        beeUseCase.getGameAnimatorBeeThree().getBeeAnimator().end();
        beeUseCase.getGameAnimatorBeeFour().getBeeAnimator().end();
        beeUseCase.getGameAnimatorBeeFive().getBeeAnimator().end();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        preferenceStoreManager = null;
        flowerUseCase = null;
        beeUseCase = null;
        currenciesUseCase = null;
        hexFieldTouchListener = null;
        hexFieldViews = null;
        hexFieldViewAnimations = null;
        beeViews = null;
        alarmManager = null;
        tikTokIntent = null;
        grid = null;
        levelOneData = null;
        flyBeeDailyTimer = null;
        flyBeeTask = null;
        visitFlowerDailyTimer = null;
        visitFlowerTask = null;
    }

}
