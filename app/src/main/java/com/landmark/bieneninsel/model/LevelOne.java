package com.landmark.bieneninsel.model;

import java.util.ArrayList;
import java.util.List;
import com.landmark.bieneninsel.ui.*;

public class LevelOne {

    private List<PlantCell> plantCells;

    public LevelOne() {
        this.plantCells = new ArrayList<>();
    }

    public void addPlantCell(HexagonImageView hexagonImageView, Integer hexFieldColor, Hex hex) {
        PlantCell plantCell = new PlantCell();
        plantCell.setId((PairID) hexagonImageView.getTag());
        plantCell.setLevel(CellLevel.EASY);
        plantCell.setCoordinatesId(hex);
        plantCell.setSoiled(false);
        plantCell.setFlourished(false);
        plantCell.setWatered(false);
        plantCell.setPollinated(false);
        plantCell.setLastTimeWatered(null);
        plantCell.setDayToGrowFlower(null);
        plantCell.setLastTimePollinated(null);
        plantCell.setColor(hexFieldColor);
        plantCell.setGroupId(0);
        plantCell.setHappy(false);
        plantCell.setMarked(false);
        plantCell.setBusy(false);
        plantCell.setTimeToUnMarkFlower(null);
        plantCells.add(plantCell);
    }

    public List<PlantCell> getPlantCells() {
        return plantCells;
    }

}