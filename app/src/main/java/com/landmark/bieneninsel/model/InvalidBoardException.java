package com.landmark.bieneninsel.model;

public class InvalidBoardException extends Exception {

    public InvalidBoardException () {

    }

    public InvalidBoardException (String message) {
        super (message);
    }

    public InvalidBoardException (Throwable cause) {
        super (cause);
    }

    public InvalidBoardException (String message, Throwable cause) {
        super (message, cause);
    }
}