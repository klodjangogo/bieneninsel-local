package com.landmark.bieneninsel.model;

import java.io.Serializable;
import java.util.Objects;

public class PairID implements Serializable {

    public Integer first;
    public Integer second;

    public PairID(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairID pairID = (PairID) o;
        return first.equals(pairID.first) &&
                second.equals(pairID.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public String toString() {
        return "PairID{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
