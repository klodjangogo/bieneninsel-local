package com.landmark.bieneninsel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

public class PlantCell implements Serializable {

    @SerializedName("id") @Expose private PairID id;
    @SerializedName("level") @Expose private CellLevel level;
    @SerializedName("coordinatesId") @Expose private Hex coordinatesId;
    @SerializedName("soiled") @Expose private Boolean soiled;
    @SerializedName("flourished") @Expose private Boolean flourished; // happy or sad
    @SerializedName("watered") @Expose private Boolean watered;
    @SerializedName("pollinated") @Expose private Boolean pollinated;
    @SerializedName("happy") @Expose private Boolean happy;
    @SerializedName("color") @Expose private Integer color;
    @SerializedName("groupId") @Expose private Integer groupId;
    @SerializedName("marked") @Expose private Boolean marked;
    @SerializedName("busy") @Expose private Boolean busy;
    @SerializedName("lastTimeWatered") @Expose private LocalDateTime lastTimeWatered;
    @SerializedName("dayToGrowFlower") @Expose private LocalDateTime dayToGrowFlower;
    @SerializedName("lastTimePollinated") @Expose private LocalDateTime lastTimePollinated;
    @SerializedName("dayToCheckFlower") @Expose private LocalDateTime dayToCheckFlower;
    @SerializedName("timeToUnMarkFlower") @Expose private LocalDateTime timeToUnMarkFlower;

    public PlantCell() { }

    public PairID getId() {
        return id;
    }

    public void setId(PairID id) {
        this.id = id;
    }

    public CellLevel getLevel() {
        return level;
    }

    public void setLevel(CellLevel level) {
        this.level = level;
    }

    public Hex getCoordinatesId() {
        return coordinatesId;
    }

    public void setCoordinatesId(Hex coordinatesId) {
        this.coordinatesId = coordinatesId;
    }

    public Boolean isSoiled() {
        return soiled;
    }

    public void setSoiled(Boolean soiled) {
        this.soiled = soiled;
    }

    public Boolean isFlourished() {
        return flourished;
    }

    public void setFlourished(Boolean flourished) {
        this.flourished = flourished;
    }

    public Boolean isWatered() {
        return watered;
    }

    public void setWatered(Boolean watered) {
        this.watered = watered;
    }

    public Boolean isPollinated() {
        return pollinated;
    }

    public void setPollinated(Boolean pollinated) {
        this.pollinated = pollinated;
    }

    public Boolean isHappy() {
        return happy;
    }

    public void setHappy(Boolean happy) {
        this.happy = happy;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Boolean isMarked() {
        return marked;
    }

    public void setMarked(Boolean marked) {
        this.marked = marked;
    }

    public LocalDateTime getLastTimeWatered() {
        return lastTimeWatered;
    }

    public void setLastTimeWatered(LocalDateTime lastTimeWatered) {
        this.lastTimeWatered = lastTimeWatered;
    }

    public LocalDateTime getDayToGrowFlower() {
        return dayToGrowFlower;
    }

    public void setDayToGrowFlower(LocalDateTime dayToGrowFlower) {
        this.dayToGrowFlower = dayToGrowFlower;
    }

    public LocalDateTime getLastTimePollinated() {
        return lastTimePollinated;
    }

    public void setLastTimePollinated(LocalDateTime lastTimePollinated) {
        this.lastTimePollinated = lastTimePollinated;
    }

    public LocalDateTime getDayToCheckFlower() {
        return dayToCheckFlower;
    }

    public void setDayToCheckFlower(LocalDateTime dayToCheckFlower) {
        this.dayToCheckFlower = dayToCheckFlower;
    }

    public LocalDateTime getTimeToUnMarkFlower() {
        return timeToUnMarkFlower;
    }

    public void setTimeToUnMarkFlower(LocalDateTime timeToUnMarkFlower) {
        this.timeToUnMarkFlower = timeToUnMarkFlower;
    }

    public Boolean isBusy() {
        return busy;
    }

    public void setBusy(Boolean busy) {
        this.busy = busy;
    }
}

