package com.landmark.bieneninsel.model;

import com.landmark.bieneninsel.R;

public class GameBoard {
    private int radius;
    private int[] validBoardDimensions;

    public GameBoard() {
        validBoardDimensions = new int[]{3, 5};
    }

    public int[][] generateGameBoard() {
        int size = 2 * radius + 1;
        int[][] nodesArray = new int[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // cutting edges
                if (radius > 1) if (carveGameBoard(i, j)) continue;
                // level 1 hex-fields
                if (radius > 1) if (setupLevelOne(i, j)) nodesArray[i][j] = R.color.groundDark;
                // level 2 hex-fields
                else if (radius > 1) if (setupLevelTwo(i, j+1)) nodesArray[i][j] = R.color.groundMedium;
                else if (radius > 1) if (setupLevelTwo(i+1, j) && i < radius) nodesArray[i][j] = R.color.groundMedium;
                else if (radius > 1) if (setupLevelTwo(i-1, j) && i > radius) nodesArray[i][j] = R.color.groundMedium;
                // level 3 hex-fieldsR.color.groundDark
                else nodesArray[i][j] = R.color.groundLight;
            }
        }
        return nodesArray;
    }

    private boolean carveGameBoard(int i, int j) {
        if (i == 0 && j == 0) return true;
        if (i == radius && j == 0) return true;
        if (i == 0 && j == radius) return true;
        if (i == radius && j == 2 * radius) return true;
        if (i == 2 * radius && j == radius) return true;
        if (i == 2 * radius && j == 0) return true;

        if (radius > 3) {
            if (i == 0 && j == 1) return true;
            if (i == 1 && j == 0) return true;
            if (i == radius + 1 && j == 2 * radius - 1) return true;
            if (i == 2 * radius - 1 && j == radius + 1) return true;
            if (i == radius - 1 && j == 0) return true;
            if (i == 0 && j == radius - 1) return true;
            if (i == radius - 1 && j == 2 * radius - 1) return true;
            if (i == radius + 1 && j == 0) return true;
            if (i == 1 && j == radius + 1) return true;
            if (i == 2 * radius - 1 && j == 0) return true;
            if (i == 2 * radius && j == 1) return true;
            if (i == 2 * radius && j == radius - 1) return true;
        }
        return false;
    }

    public void setRadius(int radius) {
        this.radius = isValidRadius(radius) ? radius : 0;
    }

    public boolean isValidRadius(int radius) {
        for (int dimension : validBoardDimensions) {
            if (dimension == radius) return true;
        }
        return false;
    }

    private boolean setupLevelOne(int i, int j) {
        if (i == radius - 2 && j == radius - 1) return true;
        if (i == radius - 1 && j == radius - 2) return true;
        if (i == radius + 1 && j == radius - 2) return true;
        if (i == radius + 2 && j == radius - 1) return true;
        if (i == radius + 1 && j == radius + 1) return true;
        if (i == radius - 1 && j == radius + 1) return true;
        if (i == radius && j == radius) return true;

        if (radius > 3) {
            if (i == 1 && j == radius - 2) return true;
            if (i == radius - 3 && j == radius - 3) return true;
            if (i == radius - 2 && j == 1) return true;
            if (i == radius && j == radius - 3) return true;
            if (i == radius + 2 && j == 1) return true;
            if (i == radius + 3 && j == radius - 3) return true;
            if (i == radius + 4 && j == radius - 2) return true;
            if (i == radius + 3 && j == radius) return true;
            if (i == radius + 2 && j == radius + 2) return true;
            if (i == radius && j == radius + 3) return true;
            if (i == radius - 2 && j == radius + 2) return true;
            if (i == radius - 3 && j == radius) return true;
        }
        return false;
    }

    private boolean setupLevelTwo(int i, int j) {
        if (i == radius - 2 && j == radius - 1) return true;
        if (i == radius - 1 && j == radius - 2) return true;
        if (i == radius + 1 && j == radius - 2) return true;
        if (i == radius + 2 && j == radius - 1) return true;
        if (i == radius + 1 && j == radius + 1) return true;
        if (i == radius - 1 && j == radius + 1) return true;
        if (i == radius && j == radius) return true;

        if (radius == 3) if (i == radius && j == 2 * radius) return true;

        if (radius > 3) {
            if (i == 1 && j == radius - 2) return true;
            if (i == radius - 3 && j == radius - 3) return true;
            if (i == radius - 2 && j == 1) return true;
            if (i == radius && j == radius - 3) return true;
            if (i == radius + 2 && j == 1) return true;
            if (i == radius + 3 && j == radius - 3) return true;
            if (i == radius + 4 && j == radius - 2) return true;
            if (i == radius + 3 && j == radius) return true;
            if (i == radius + 2 && j == radius + 2) return true;
            if (i == radius && j == radius + 3) return true;
            if (i == radius - 2 && j == radius + 2) return true;
            if (i == radius - 3 && j == radius) return true;
        }
        return false;
    }

}
