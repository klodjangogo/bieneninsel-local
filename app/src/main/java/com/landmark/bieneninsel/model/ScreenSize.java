package com.landmark.bieneninsel.model;

public enum ScreenSize {
    LARGE, NORMAL, SMALL
}
