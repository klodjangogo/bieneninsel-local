package com.landmark.bieneninsel.model;

public enum CellLevel {
    EASY,
    MEDIUM,
    HARD
}
