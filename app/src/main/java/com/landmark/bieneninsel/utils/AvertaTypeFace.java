package com.landmark.bieneninsel.utils;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public interface AvertaTypeFace {
    String AVERTA_LIGHT = "averta_light.otf";
    String AVERTA_REGULAR = "averta_regular.otf";
    String AVERTA_SEMIBOLD = "averta_semibold.otf";
    String AVERTA_BOLD = "averta_bold.otf";
    String AVERTA_EXTRABOLD = "averta_extra_bold.otf";

    void setAvertaTypeface(@Typeface String fontName);

    @Retention(SOURCE)
    @StringDef({AVERTA_LIGHT, AVERTA_REGULAR, AVERTA_SEMIBOLD, AVERTA_BOLD, AVERTA_EXTRABOLD})
    @interface Typeface {
    }

}
