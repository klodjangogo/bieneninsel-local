package com.landmark.bieneninsel.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    // finds the diff between two list of elements
    public static <T> List<T> difference(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<>();

        for (T t : list1) {
            if (!list2.contains(t)) list.add(t);
        }

        return list;
    }
}
