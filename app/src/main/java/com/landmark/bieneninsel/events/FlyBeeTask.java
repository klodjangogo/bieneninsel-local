package com.landmark.bieneninsel.events;

import android.app.Activity;
import android.content.Context;

import com.landmark.bieneninsel.data.Constants;
import com.landmark.bieneninsel.usecases.BeeUseCase;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class FlyBeeTask extends TimerTask {
    private Context context;
    private Timer timer;
    private Random random;
    private BeeUseCase beeUseCase;

    public FlyBeeTask(Context context, Timer timer, BeeUseCase beeUseCase) {
        this.context = context;
        this.random = new Random();
        this.timer = timer;
        this.beeUseCase = beeUseCase;
    }

    @Override
    public void run() {
        ((Activity) context).runOnUiThread(() -> beeUseCase.chooseRandomBee());
        int randomTime = random.nextInt(Constants.RANDOM_TIME_TO_CHOOSE_BEE);
//        int randomTime = random.nextInt(300000); // TODO: delete for release, demo 5 mins
        printLog(randomTime);
        timer.schedule(new FlyBeeTask(context, timer, beeUseCase), randomTime);
    }

    // TODO: remove logs when doing release builds
    void printLog(int randomTime) {
        long minutes = (randomTime / 1000) / 60;
        int seconds = (int) ((randomTime / 1000) % 60);
        System.out.println("Bieneninsel - Choose next random bee in: " + minutes + " mins and " + seconds + " seconds");
    }

}
