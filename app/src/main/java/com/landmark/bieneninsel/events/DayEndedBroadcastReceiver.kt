package com.landmark.bieneninsel.events

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.landmark.bieneninsel.data.Constants
import org.threeten.bp.LocalTime
import java.text.SimpleDateFormat
import java.util.*

abstract class DayEndedBroadcastReceiver : BroadcastReceiver() {

    private var date = Date()
    private val dateFormat by lazy { SimpleDateFormat("yyMMdd", Locale.getDefault()) }

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        val currentDate = Date()
        val timeNow: LocalTime? = LocalTime.now()

//        if ((action == Intent.ACTION_TIME_CHANGED || action == Intent.ACTION_TIMEZONE_CHANGED) && isSameDay(currentDate) && timeNow?.hour == Constants.DAY_TIME_END) {
//            date = currentDate
//            onDayEnded()
//        }

        // TODO: delete for release
        if (timeNow?.hour == 15 && timeNow.minute == 55) {
            date = currentDate
            onDayEnded()
        }
    }

    private fun isSameDay(currentDate: Date) = dateFormat.format(currentDate) == dateFormat.format(date)

    abstract fun onDayEnded()

    companion object {

        /**
         * Create the [IntentFilter] for the [DayEndedBroadcastReceiver].
         *
         * @return The [IntentFilter]
         */
        fun getIntentFilter() = IntentFilter().apply {
            addAction(Intent.ACTION_TIME_TICK)
            addAction(Intent.ACTION_TIMEZONE_CHANGED)
            addAction(Intent.ACTION_TIME_CHANGED)
        }
    }
}