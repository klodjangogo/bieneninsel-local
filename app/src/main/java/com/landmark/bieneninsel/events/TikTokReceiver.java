package com.landmark.bieneninsel.events;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class TikTokReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Integer flowerIdFirst = (Integer) intent.getIntExtra("FLOWER_ID_FIRST", -1000);
        Integer flowerIdSecond = (Integer) intent.getIntExtra("FLOWER_ID_SECOND", -1000);
        Intent alarmIntent = new Intent("ALARM_TO_UPDATE_FLOWER");
        alarmIntent.putExtra("FLOWER_ID_FIRST", flowerIdFirst);
        alarmIntent.putExtra("FLOWER_ID_SECOND", flowerIdSecond);
        context.sendBroadcast(alarmIntent);
    }

}
