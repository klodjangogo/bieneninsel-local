package com.landmark.bieneninsel.events;

import android.app.Activity;
import android.content.Context;

import com.landmark.bieneninsel.data.Constants;
import com.landmark.bieneninsel.usecases.BeeUseCase;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class VisitFlowerTask extends TimerTask {
    private Context context;
    private Timer timer;
    private Random random;
    private BeeUseCase beeUseCase;

    public VisitFlowerTask(Context context, Timer timer, BeeUseCase beeUseCase) {
        this.context = context;
        this.random = new Random();
        this.timer = timer;
        this.beeUseCase = beeUseCase;
    }

    @Override
    public void run() {
        // choose a random time from next 30 minutes and call a random bee to visit a random marked happy (ready) flower
        int randomTime = random.nextInt(Constants.RANDOM_TIME_TO_CALL_BEE_TO_VISIT_FLOWER);
//        int randomTime = random.nextInt(30000); // demo 1 mins
        printLog(randomTime);
        timer.schedule(new VisitFlowerTask(context, timer, beeUseCase), randomTime);
        ((Activity) context).runOnUiThread(() -> beeUseCase.visitRandomFlower());

    }

    // TODO: remove logs when doing release builds
    void printLog(int randomTime) {
        long minutes = (randomTime / 1000) / 60;
        int seconds = (int) ((randomTime / 1000) % 60);
        System.out.println("Bieneninsel - Choose next random flower to visit in: " + minutes + " mins and " + seconds + " seconds");
    }

}
