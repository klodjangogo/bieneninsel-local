package com.landmark.bieneninsel.events;

public interface WateringInProgressListener {
    void isWatering(boolean flag);
}
