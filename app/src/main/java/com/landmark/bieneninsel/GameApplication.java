package com.landmark.bieneninsel;

import android.app.Application;
import android.content.res.Configuration;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.landmark.bieneninsel.data.GameData;

public class GameApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        GameData.initialize(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}