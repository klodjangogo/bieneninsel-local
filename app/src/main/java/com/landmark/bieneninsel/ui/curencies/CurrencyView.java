package com.landmark.bieneninsel.ui.curencies;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.landmark.bieneninsel.R;
import com.landmark.bieneninsel.utils.AvertaTypeFace;

import static com.landmark.bieneninsel.utils.AvertaTypeFace.AVERTA_BOLD;
import static com.landmark.bieneninsel.utils.AvertaTypeFace.AVERTA_EXTRABOLD;
import static com.landmark.bieneninsel.utils.AvertaTypeFace.AVERTA_LIGHT;
import static com.landmark.bieneninsel.utils.AvertaTypeFace.AVERTA_REGULAR;
import static com.landmark.bieneninsel.utils.AvertaTypeFace.AVERTA_SEMIBOLD;

public class CurrencyView extends LinearLayout {

    private LinearLayout backgroundView;
    private ImageView symbolView;
    private TextView pointsTextView;

    public CurrencyView(Context context) {
        super(context);
        initView(context);
    }

    public CurrencyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
        getAttrs(context, attrs);
    }

    public CurrencyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
        getAttrs(context, attrs, defStyleAttr);
    }

    private void initView(Context context) {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.currency_layout_view, this, false);
        addView(v);

        backgroundView = (LinearLayout) findViewById(R.id.bg);
        pointsTextView = (TextView) findViewById(R.id.pointsTextView);
        symbolView = (ImageView) findViewById(R.id.symbol);
    }

    private void getAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CurrencyView);
        setTypeArray(context, typedArray);
    }

    private void getAttrs(Context context, @Nullable AttributeSet attrs, int defStyle) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CurrencyView, defStyle, 0);
        setTypeArray(context, typedArray);
    }

    private void setTypeArray(Context context, TypedArray typedArray) {
        int bg_resID = typedArray.getResourceId(R.styleable.CurrencyView_bg_drawable, R.drawable.blutenstaub_currency_bg);
        backgroundView.setBackgroundResource(bg_resID);

        int symbol_resID = typedArray.getResourceId(R.styleable.CurrencyView_symbol, R.drawable.blutenstaub);
        symbolView.setImageResource(symbol_resID);

        int textColor = typedArray.getColor(R.styleable.CurrencyView_text_color, 0);
        pointsTextView.setTextColor(textColor);

        String text_string = typedArray.getString(R.styleable.CurrencyView_text_string);
        pointsTextView.setText(text_string);

        String customFont = typedArray.getString(R.styleable.CurrencyView_text_typeface);
        setCustomFont(pointsTextView, context, customFont);

        typedArray.recycle();
    }

    public void setBg(int bg_resID) {
        backgroundView.setBackgroundResource(bg_resID);
    }

    public void setSymbol(int symbol_resID) {
        symbolView.setImageResource(symbol_resID);
    }

    public void setTextColor(int color) {
        pointsTextView.setTextColor(color);
    }

    public void setText(String text_string) {
        pointsTextView.setText(text_string);
    }

    public void setText(int text_resID) {
        pointsTextView.setText(text_resID);
    }

    public void setCustomFont(TextView v, Context ctx, String font) {
        android.graphics.Typeface typeface = null;
        try {
            if (font == null) font = AVERTA_REGULAR;
            typeface = android.graphics.Typeface.createFromAsset(ctx.getAssets(), "fonts/" + font);
        } catch (Exception e) {
            typeface = android.graphics.Typeface.createFromAsset(ctx.getAssets(), "fonts/" + AVERTA_REGULAR);
        }
        v.setTypeface(typeface);
    }

    public void setAvertaTypeface(TextView v, @AvertaTypeFace.Typeface String fontName) {
        String selectedFont = AVERTA_REGULAR;
        switch (fontName) {
            case AVERTA_LIGHT:
                selectedFont = AVERTA_LIGHT;
                break;
            case AVERTA_REGULAR:
                selectedFont = AVERTA_REGULAR;
                break;
            case AVERTA_SEMIBOLD:
                selectedFont = AVERTA_SEMIBOLD;
                break;
            case AVERTA_BOLD:
                selectedFont = AVERTA_BOLD;
                break;
            case AVERTA_EXTRABOLD:
                selectedFont = AVERTA_EXTRABOLD;
                break;

        }
        android.graphics.Typeface typeface = android.graphics.Typeface.createFromAsset(v.getContext().getAssets(),
                "fonts/" + selectedFont);
        v.setTypeface(typeface);
    }

}
