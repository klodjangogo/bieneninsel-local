package com.landmark.bieneninsel.ui.listener;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.landmark.bieneninsel.model.Grid;
import com.landmark.bieneninsel.usecases.FlowerUseCase;

public class HexFieldTouchListener implements View.OnTouchListener {
    private GestureDetector gestureDetector;
    private HexFieldGestureListener gestureListener;

    public HexFieldTouchListener(Context context) {
        this.gestureListener = new HexFieldGestureListener(context);
        this.gestureDetector = new GestureDetector(context, gestureListener);
    }

    public void setGrid(Grid grid) {
        gestureListener.setGrid(grid);
    }

    public void setFlowerUseCase(FlowerUseCase flowerUseCase) {
        gestureListener.setFlowerUseCase(flowerUseCase);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // pass the events to the gesture detector
        // a return value of true means the detector is handling it
        // a return value of false means the detector didn't recognize the event
        gestureListener.setHexFieldView(v);
        return gestureDetector.onTouchEvent(event);

    }

}
