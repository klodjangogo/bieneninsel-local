package com.landmark.bieneninsel.ui.listener;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.landmark.bieneninsel.events.WateringInProgressListener;
import com.landmark.bieneninsel.model.Grid;
import com.landmark.bieneninsel.model.PlantCell;
import com.landmark.bieneninsel.ui.HexagonImageView;
import com.landmark.bieneninsel.usecases.FlowerUseCase;

public class HexFieldGestureListener extends GestureDetector.SimpleOnGestureListener {
    private Context context;
    private FlowerUseCase flowerUseCase;
    private HexagonImageView hexagonImageView;
    private PlantCell plantCell;
    private Grid grid;
    private boolean isThrowingWater;

    public HexFieldGestureListener(Context context) {
        this.context = context;
    }

    public void setGrid(final Grid grid) {
        this.grid = grid;
    }

    public void setHexFieldView(final View view) {
        if (!isThrowingWater) {
            hexagonImageView = (HexagonImageView) view;
            plantCell = flowerUseCase.getPlantCell(hexagonImageView);
        }
    }

    public void setFlowerUseCase(FlowerUseCase flowerUseCase) {
        this.flowerUseCase = flowerUseCase;
        flowerUseCase.setWateringInProgressListener(new WateringInProgressListener() {
            @Override
            public void isWatering(boolean flag) {
                isThrowingWater = flag;
            }
        });
    }

    // don't return false here or else none of the other gestures will work
    @Override
    public boolean onDown(MotionEvent event) {
        float xPoint = event.getX();
        float yPoint = event.getY();
        boolean isPointOutOfCircle = (grid.centerOffsetX - xPoint) * (grid.centerOffsetX - xPoint) + (grid.centerOffsetY - yPoint) * (grid.centerOffsetY - yPoint) > grid.width * grid.width / 4;
        if (isPointOutOfCircle) return false;
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        if (plantCell != null) {
            if (!plantCell.isSoiled() && !plantCell.isFlourished() && !isThrowingWater) { // work the land
                flowerUseCase.createSoil(hexagonImageView, plantCell);
                flowerUseCase.setHexFieldGroup(plantCell); // define groups of flowers
                flowerUseCase.lockLevelOne(hexagonImageView); // impose one flower/day rule
            }
            if (plantCell.isHappy() && plantCell.isFlourished() && !plantCell.isMarked() && !isThrowingWater) { // mark as target for pollen collection
                flowerUseCase.markFlowerForHarvesting(hexagonImageView, plantCell);
            }
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        if (plantCell != null) {
            if (plantCell.isSoiled() || plantCell.isFlourished()) { // you can throw water only when soil or flourished
                if (!plantCell.isWatered() && !isThrowingWater) { // throw water
                    flowerUseCase.startThrowWater(hexagonImageView, plantCell);
                }
            }
        }
    }

}
