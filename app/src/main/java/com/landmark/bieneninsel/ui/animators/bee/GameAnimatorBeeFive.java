package com.landmark.bieneninsel.ui.animators.bee;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import com.landmark.bieneninsel.data.Constants;

public class GameAnimatorBeeFive {

    private View beeView;
    private int xDestination;
    private int yDestination;

    public GameAnimatorBeeFive() {
    }

    public Animator getBeeAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = getTranslationXAnimator(new DecelerateInterpolator(1f));
        Animator yAnimator = getTranslationYAnimator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_CHANGE_DIRECTION);
        return animatorSet;
    }

    private Animator getTranslationXAnimator(Interpolator interpolator) {
        Animator animator = ObjectAnimator.ofFloat(beeView, "translationX", xDestination);
        animator.setInterpolator(interpolator);
        return animator;
    }

    private Animator getTranslationYAnimator(Interpolator interpolator) {
        Animator animator = ObjectAnimator.ofFloat(beeView, "translationY", yDestination);
        animator.setInterpolator(interpolator);
        return animator;
    }

    public void setBeeView(View beeView) {
        this.beeView = beeView;
    }

    public void setxDestination(int xDestination) {
        this.xDestination = xDestination;
    }

    public void setyDestination(int yDestination) {
        this.yDestination = yDestination;
    }
}
