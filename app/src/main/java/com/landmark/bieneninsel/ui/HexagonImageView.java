package com.landmark.bieneninsel.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.landmark.bieneninsel.model.Hex;

public class HexagonImageView extends AppCompatImageView {

    private Path hexagonPath;
    private float radius;
    private int viewWidth;
    private int viewHeight;
    private Paint paint;
    private LinearGradient shader;
    private int borderWidth = 0;
    private Hex mHex; // Hold the node coordinates on the grid

    public HexagonImageView(Context context) {
        super(context);
        setup();
    }

    public HexagonImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public HexagonImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public void setup() {
        paint = new Paint();
        paint.setAntiAlias(true);
        shader = new LinearGradient(0, 0, getWidth(), getHeight(), Color.TRANSPARENT, Color.TRANSPARENT, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        this.setLayerType(LAYER_TYPE_SOFTWARE, paint);
        hexagonPath = new Path();
    }

    public void setBgColor(int bgColor) {
        if (bgColor != 0) {
            shader = new LinearGradient(0, 0, getWidth(), getHeight(),
                    getContext().getResources().getColor(bgColor),
                    getContext().getResources().getColor(bgColor),
                    Shader.TileMode.CLAMP);
            paint.setShader(shader);
        }
        this.invalidate();
    }

    private void calculatePath() {
        float centerX = viewWidth / 2;
        float centerY = viewHeight / 2;
        float radiusBorder = radius - borderWidth;
        float triangleBorderHeight = (float) (Math.sqrt(3) * radiusBorder / 2);
        hexagonPath.moveTo(centerX, centerY + radiusBorder);
        hexagonPath.lineTo(centerX - triangleBorderHeight, centerY + radiusBorder / 2);
        hexagonPath.lineTo(centerX - triangleBorderHeight, centerY - radiusBorder / 2);
        hexagonPath.lineTo(centerX, centerY - radiusBorder);
        hexagonPath.lineTo(centerX + triangleBorderHeight, centerY - radiusBorder / 2);
        hexagonPath.lineTo(centerX + triangleBorderHeight, centerY + radiusBorder / 2);
        hexagonPath.moveTo(centerX, centerY + radiusBorder);
        invalidate();
    }

    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(hexagonPath, paint);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = measureWidth(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec, widthMeasureSpec);
        viewWidth = width - (borderWidth * 2);
        viewHeight = height - (borderWidth * 2);
        radius = height / 2 - borderWidth;
        calculatePath();
        setMeasuredDimension(width, height);
    }

    private int measureWidth(int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = viewWidth;
        }
        return result;
    }

    private int measureHeight(int measureSpecHeight, int measureSpecWidth) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpecHeight);
        int specSize = MeasureSpec.getSize(measureSpecHeight);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = viewHeight;
        }
        return (result + 2);
    }

    public Hex getHex() {
        return mHex;
    }

    public void setHex(Hex hex) {
        mHex = hex;
    }

}