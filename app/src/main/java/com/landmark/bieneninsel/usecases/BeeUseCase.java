package com.landmark.bieneninsel.usecases;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.landmark.bieneninsel.data.Constants;
import com.landmark.bieneninsel.ui.HexagonImageView;
import com.landmark.bieneninsel.ui.animators.bee.GameAnimatorBeeFive;
import com.landmark.bieneninsel.ui.animators.bee.GameAnimatorBeeFour;
import com.landmark.bieneninsel.ui.animators.bee.GameAnimatorBeeOne;
import com.landmark.bieneninsel.ui.animators.bee.GameAnimatorBeeThree;
import com.landmark.bieneninsel.ui.animators.bee.GameAnimatorBeeTwo;
import com.landmark.bieneninsel.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.landmark.bieneninsel.R;

public class BeeUseCase {

    private Context context;
    private FlowerUseCase flowerUseCase;
    private List<LottieAnimationView> bees; // 5 bees for Level 1
    private List<View> pivotPoints; // 13 points in screen used to make random bee fly
    private GameAnimatorBeeOne gameAnimatorBeeOne;
    private GameAnimatorBeeTwo gameAnimatorBeeTwo;
    private GameAnimatorBeeThree gameAnimatorBeeThree;
    private GameAnimatorBeeFour gameAnimatorBeeFour;
    private GameAnimatorBeeFive gameAnimatorBeeFive;
    private Random random;
    private List<LottieAnimationView> flyingBees;
    private View exitDoor;
    private LottieAnimationView beeOneView, beeTwoView, beeThreeView, beeFourView, beeFiveView;
    private CountDownTimer beeOneTimer, beeTwoTimer, beeThreeTimer, beeFourTimer, beeFiveTimer;
    private int beeOffsetX;
    private int beeOffsetY;

    public BeeUseCase(Context context, FlowerUseCase flowerUseCase, List<LottieAnimationView> beeViews, View exitDoor) {
        this.context = context;
        this.flowerUseCase = flowerUseCase;
        this.bees = beeViews;
        this.beeOneView = beeViews.get(0);
        this.beeTwoView = beeViews.get(1);
        this.beeThreeView = beeViews.get(2);
        this.beeFourView = beeViews.get(3);
        this.beeFiveView = beeViews.get(4);
        this.exitDoor = exitDoor;
        this.gameAnimatorBeeOne = new GameAnimatorBeeOne();
        this.gameAnimatorBeeTwo = new GameAnimatorBeeTwo();
        this.gameAnimatorBeeThree = new GameAnimatorBeeThree();
        this.gameAnimatorBeeFour = new GameAnimatorBeeFour();
        this.gameAnimatorBeeFive = new GameAnimatorBeeFive();
        this.random = new Random();
        this.flyingBees = new ArrayList<>();

        boolean tablet = context.getResources().getBoolean(R.bool.isOver600); //a 10” tablet (720x1280 mdpi, 800x1280 mdpi, etc). or a 7” tablet (600x1024 mdpi).

        TypedValue beeOffsetXValue = new TypedValue();
        context.getResources().getValue(R.dimen.beeOffsetX, beeOffsetXValue, true);
        float beeOffsetXPhone = beeOffsetXValue.getFloat();

        TypedValue beeOffsetYValue = new TypedValue();
        context.getResources().getValue(R.dimen.beeOffsetY, beeOffsetYValue, true);
        float beeOffsetYPhone = beeOffsetYValue.getFloat();

        if (tablet) {
            this.beeOffsetX = context.getResources().getInteger(R.integer.beeOffsetX);
            this.beeOffsetY = context.getResources().getInteger(R.integer.beeOffsetY);
        } else {
            this.beeOffsetX = (int) beeOffsetXPhone;
            this.beeOffsetY = (int) beeOffsetYPhone;
        }

    }

    public void setPivotPoints(List<View> pivotPoints) {
        this.pivotPoints = pivotPoints;
    }

    public GameAnimatorBeeOne getGameAnimatorBeeOne() {
        return gameAnimatorBeeOne;
    }

    public GameAnimatorBeeTwo getGameAnimatorBeeTwo() {
        return gameAnimatorBeeTwo;
    }

    public GameAnimatorBeeThree getGameAnimatorBeeThree() {
        return gameAnimatorBeeThree;
    }

    public GameAnimatorBeeFour getGameAnimatorBeeFour() {
        return gameAnimatorBeeFour;
    }

    public GameAnimatorBeeFive getGameAnimatorBeeFive() {
        return gameAnimatorBeeFive;
    }

    public List<LottieAnimationView> getFlyingBees() {
        return flyingBees;
    }

    // ----------------------------   BEE ENTERING AND FLYING AROUND   ---------------------------- //

    /* For a RANDOM period between 1 to 30 mins do:
        - pick a RANDOM bee from the group of available bees (5 bees for Level 1)
        - fly bee around Bienensinel along a RANDOM path */
    public void chooseRandomBee() {
        List<LottieAnimationView> availableBees = Utils.difference(bees, flyingBees);
        if (availableBees.size() > 0) {
            int beeIndex = random.nextInt(availableBees.size());
            LottieAnimationView randomBee = availableBees.get(beeIndex);
            flyingBees.add(randomBee);
            randomBee.setBackgroundResource(0);
            randomBee.resumeAnimation();

            if (randomBee.getId() == R.id.beeOne) flyInBeeOne();
            if (randomBee.getId() == R.id.beeTwo) flyInBeeTwo();
            if (randomBee.getId() == R.id.beeThree) flyInBeeThree();
            if (randomBee.getId() == R.id.beeFour) flyInBeeFour();
            if (randomBee.getId() == R.id.beeFive) flyInBeeFive();
        }
    }

    // fly the RANDOM bee using a RANDOM path, for a RANDOM time in the scope of 1-30 minutes
    private void flyInBeeOne() {
        // fly for a random period between 1-30 minutes and change destination every 3 sec
        int randomFlyTime = random.nextInt(Constants.RANDOM_TIME_TO_FLY_BEE);
//        int randomFlyTime = random.nextInt(180000); // demo 3 mins  // TODO: delete for release
        printFlyInLog(beeOneView, randomFlyTime);
        gameAnimatorBeeOne.setBeeView(beeOneView);
        beeOneTimer = new CountDownTimer(randomFlyTime, Constants.TIME_TO_CHANGE_DIRECTION) {

            public void onTick(long millisUntilFinished) {
                Collections.shuffle(pivotPoints);
                View randomHexCellView = pivotPoints.get(random.nextInt(13));
                int[] hexagonCellPosition = new int[2];
                randomHexCellView.getLocationOnScreen(hexagonCellPosition);
                gameAnimatorBeeOne.setxDestination(hexagonCellPosition[0]);
                gameAnimatorBeeOne.setyDestination(hexagonCellPosition[1]);
                gameAnimatorBeeOne.getBeeAnimator().start();
            }

            public void onFinish() {
                flyOutBeeOne();
            }

        }.start();
    }

    // fly the RANDOM bee using a RANDOM path, for a RANDOM time in the scope of 1-30 minutes
    private void flyInBeeTwo() {
        // fly for a random period between 1-30 minutes and change destination every 3 sec
        int randomFlyTime = random.nextInt(Constants.RANDOM_TIME_TO_FLY_BEE);
//        int randomFlyTime = random.nextInt(180000); // demo 3 mins  // TODO: delete for release
        printFlyInLog(beeTwoView, randomFlyTime);
        gameAnimatorBeeTwo.setBeeView(beeTwoView);
        beeTwoTimer = new CountDownTimer(randomFlyTime, Constants.TIME_TO_CHANGE_DIRECTION) {

            public void onTick(long millisUntilFinished) {
                Collections.shuffle(pivotPoints);
                View randomHexCellView = pivotPoints.get(random.nextInt(13));
                int[] hexagonCellPosition = new int[2];
                randomHexCellView.getLocationOnScreen(hexagonCellPosition);
                gameAnimatorBeeTwo.setxDestination(hexagonCellPosition[0]);
                gameAnimatorBeeTwo.setyDestination(hexagonCellPosition[1]);
                gameAnimatorBeeTwo.getBeeAnimator().start();
            }

            public void onFinish() {
                flyOutBeeTwo();
            }

        }.start();
    }

    // fly the RANDOM bee using a RANDOM path, for a RANDOM time in the scope of 1-30 minutes
    private void flyInBeeThree() {
        // fly for a random period between 1-30 minutes and change destination every 3 sec
        int randomFlyTime = random.nextInt(Constants.RANDOM_TIME_TO_FLY_BEE);
//        int randomFlyTime = random.nextInt(180000); // demo 3 mins  // TODO: delete for release
        printFlyInLog(beeThreeView, randomFlyTime);
        gameAnimatorBeeThree.setBeeView(beeThreeView);
        beeThreeTimer = new CountDownTimer(randomFlyTime, Constants.TIME_TO_CHANGE_DIRECTION) {

            public void onTick(long millisUntilFinished) {
                Collections.shuffle(pivotPoints);
                View randomHexCellView = pivotPoints.get(random.nextInt(13));
                int[] hexagonCellPosition = new int[2];
                randomHexCellView.getLocationOnScreen(hexagonCellPosition);
                gameAnimatorBeeThree.setxDestination(hexagonCellPosition[0]);
                gameAnimatorBeeThree.setyDestination(hexagonCellPosition[1]);
                gameAnimatorBeeThree.getBeeAnimator().start();
            }

            public void onFinish() {
                flyOutBeeThree();
            }

        }.start();
    }

    // fly the RANDOM bee using a RANDOM path, for a RANDOM time in the scope of 1-30 minutes
    private void flyInBeeFour() {
        // fly for a random period between 1-30 minutes and change destination every 3 sec
        int randomFlyTime = random.nextInt(Constants.RANDOM_TIME_TO_FLY_BEE);
//        int randomFlyTime = random.nextInt(180000); // demo 3 mins  // TODO: delete for release
        printFlyInLog(beeFourView, randomFlyTime);
        gameAnimatorBeeFour.setBeeView(beeFourView);
        beeFourTimer = new CountDownTimer(randomFlyTime, Constants.TIME_TO_CHANGE_DIRECTION) {

            public void onTick(long millisUntilFinished) {
                Collections.shuffle(pivotPoints);
                View randomHexCellView = pivotPoints.get(random.nextInt(13));
                int[] hexagonCellPosition = new int[2];
                randomHexCellView.getLocationOnScreen(hexagonCellPosition);
                gameAnimatorBeeFour.setxDestination(hexagonCellPosition[0]);
                gameAnimatorBeeFour.setyDestination(hexagonCellPosition[1]);
                gameAnimatorBeeFour.getBeeAnimator().start();
            }

            public void onFinish() {
                flyOutBeeFour();
            }

        }.start();
    }

    // fly the RANDOM bee using a RANDOM path, for a RANDOM time in the scope of 1-30 minutes
    private void flyInBeeFive() {
        // fly for a random period between 1-30 minutes and change destination every 3 sec
        int randomFlyTime = random.nextInt(Constants.RANDOM_TIME_TO_FLY_BEE);
//        int randomFlyTime = random.nextInt(180000); // demo 3 mins  // TODO: delete for release
        printFlyInLog(beeFiveView, randomFlyTime);
        gameAnimatorBeeFive.setBeeView(beeFiveView);
        beeFiveTimer = new CountDownTimer(randomFlyTime, Constants.TIME_TO_CHANGE_DIRECTION) {

            public void onTick(long millisUntilFinished) {
                Collections.shuffle(pivotPoints);
                View randomHexCellView = pivotPoints.get(random.nextInt(13));
                int[] hexagonCellPosition = new int[2];
                randomHexCellView.getLocationOnScreen(hexagonCellPosition);
                gameAnimatorBeeFive.setxDestination(hexagonCellPosition[0]);
                gameAnimatorBeeFive.setyDestination(hexagonCellPosition[1]);
                gameAnimatorBeeFive.getBeeAnimator().start();
            }

            public void onFinish() {
                flyOutBeeFive();
            }

        }.start();
    }

    private void flyOutBeeOne() {
        beeOneTimer = null;
        int[] exitDoorPosition = new int[2];
        exitDoor.getLocationOnScreen(exitDoorPosition);

        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeOneView, "translationX", exitDoorPosition[0]);
        Animator yAnimator = ObjectAnimator.ofFloat(beeOneView, "translationY", exitDoorPosition[1]);
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_FLY_OUT);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                printFlyOutLog(beeOneView);
                gameAnimatorBeeOne.getBeeAnimator().end();
                beeOneView.pauseAnimation();
                flyingBees.remove(beeOneView);
            }
        });
        animatorSet.start();
    }

    private void flyOutBeeTwo() {
        beeTwoTimer = null;
        int[] exitDoorPosition = new int[2];
        exitDoor.getLocationOnScreen(exitDoorPosition);

        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeTwoView, "translationX", exitDoorPosition[0]);
        Animator yAnimator = ObjectAnimator.ofFloat(beeTwoView, "translationY", exitDoorPosition[1]);
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_FLY_OUT);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                printFlyOutLog(beeTwoView);
                gameAnimatorBeeTwo.getBeeAnimator().end();
                beeTwoView.pauseAnimation();
                flyingBees.remove(beeTwoView);
            }
        });
        animatorSet.start();
    }

    private void flyOutBeeThree() {
        beeThreeTimer = null;
        int[] exitDoorPosition = new int[2];
        exitDoor.getLocationOnScreen(exitDoorPosition);

        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeThreeView, "translationX", exitDoorPosition[0]);
        Animator yAnimator = ObjectAnimator.ofFloat(beeThreeView, "translationY", exitDoorPosition[1]);
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_FLY_OUT);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                printFlyOutLog(beeThreeView);
                gameAnimatorBeeThree.getBeeAnimator().end();
                beeThreeView.pauseAnimation();
                flyingBees.remove(beeThreeView);
            }
        });
        animatorSet.start();
    }

    private void flyOutBeeFour() {
        beeFourTimer = null;
        int[] exitDoorPosition = new int[2];
        exitDoor.getLocationOnScreen(exitDoorPosition);

        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeFourView, "translationX", exitDoorPosition[0]);
        Animator yAnimator = ObjectAnimator.ofFloat(beeFourView, "translationY", exitDoorPosition[1]);
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_FLY_OUT);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                printFlyOutLog(beeFourView);
                gameAnimatorBeeFour.getBeeAnimator().end();
                beeFourView.pauseAnimation();
                flyingBees.remove(beeFourView);
            }
        });
        animatorSet.start();
    }

    private void flyOutBeeFive() {
        beeFiveTimer = null;
        int[] exitDoorPosition = new int[2];
        exitDoor.getLocationOnScreen(exitDoorPosition);

        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeFiveView, "translationX", exitDoorPosition[0]);
        Animator yAnimator = ObjectAnimator.ofFloat(beeFiveView, "translationY", exitDoorPosition[1]);
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_FLY_OUT);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                printFlyOutLog(beeFiveView);
                gameAnimatorBeeFive.getBeeAnimator().end();
                beeFiveView.pauseAnimation();
                flyingBees.remove(beeFiveView);
            }
        });
        animatorSet.start();
    }

    public void exitBees() {
        for (View bee : flyingBees) {
            if (bee.getId() == R.id.beeOne) flyOutBeeOne();
            if (bee.getId() == R.id.beeTwo) flyOutBeeTwo();
            if (bee.getId() == R.id.beeThree) flyOutBeeThree();
            if (bee.getId() == R.id.beeFour) flyOutBeeFour();
            if (bee.getId() == R.id.beeFive) flyOutBeeFive();
        }
    }

    // TODO: remove logs when doing release builds
    void printFlyInLog(View randomBee, int randomTime) {
        long minutes = (randomTime / 1000) / 60;
        int seconds = (int) ((randomTime / 1000) % 60);
        System.out.println("Bieneninsel - " + randomBee.getTag() + " flying for: " + minutes + " mins and " + seconds + " seconds");
    }

    // TODO: remove logs when doing release builds
    void printFlyOutLog(View randomBee) {
        System.out.println("Bieneninsel - " + randomBee.getTag() + " flying out");
    }

    // -----------------------------------    BEE VISIT FLOWER   ----------------------------------- //

     /* For a RANDOM period between 1 to 30 mins do:
        - pick a RANDOM marked flower from the group of happy flowers
        - pick a RANDOM bee from the group of flying bees
        - fly bee to the flower for pollen collection */
    public void visitRandomFlower() {
        if (flowerUseCase.getReadyFlowers().size() > 0 && flyingBees.size() > 0) {
            int beeIndex = random.nextInt(flyingBees.size());
            View randomBee = flyingBees.get(beeIndex);
            int readyFlowerIndex = random.nextInt(flowerUseCase.getReadyFlowers().size());
            HexagonImageView randomReadyFlower = (HexagonImageView) flowerUseCase.getReadyFlowers().get(readyFlowerIndex);
            // remove ready flower after bee-flower match is done so that another bee doesn't visit same flower at same time
            flowerUseCase.removeReadyFlower(randomReadyFlower);
            flowerUseCase.setBusyFlower(randomReadyFlower, true);

            if (randomBee.getId() == R.id.beeOne) visitFlowerBeeOne(randomReadyFlower);
            if (randomBee.getId() == R.id.beeTwo) visitFlowerBeeTwo(randomReadyFlower);
            if (randomBee.getId() == R.id.beeThree) visitFlowerBeeThree(randomReadyFlower);
            if (randomBee.getId() == R.id.beeFour) visitFlowerBeeFour(randomReadyFlower);
            if (randomBee.getId() == R.id.beeFive) visitFlowerBeeFive(randomReadyFlower);
        }
    }

     /* Fly bee to the flower for pollen collection.
        - bee randomly land on marked flowers (1 bee per flower at a time)
        - bee collect pollen for a time period (30 sec)
        - bee leaves the board Level 1 */
    private void visitFlowerBeeOne(HexagonImageView randomReadyFlower) {
        beeOneTimer.cancel();
        gameAnimatorBeeOne.getBeeAnimator().cancel();
        flyingBees.remove(beeOneView);
        if (gameAnimatorBeeOne.getBeeAnimator().isRunning()) return;

        int[] randomReadyFlowerPosition = new int[2];
        randomReadyFlower.getLocationOnScreen(randomReadyFlowerPosition);
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeOneView, "translationX", randomReadyFlowerPosition[0] + beeOffsetX);
        Animator yAnimator = ObjectAnimator.ofFloat(beeOneView, "translationY", randomReadyFlowerPosition[1] - beeOffsetY);
        xAnimator.setInterpolator(new DecelerateInterpolator(1f));
        yAnimator.setInterpolator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_LAND_ON_FLOWER);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                beeOneView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.glowing_bee));
                // bee stay on flower for 30 sec collecting pollen, then fly out
                new CountDownTimer(Constants.TIME_TO_STAY_ON_FLOWER, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) { }

                    @Override
                    public void onFinish() {
                        flowerUseCase.onHarvestingFlowerFinished(randomReadyFlower);
                        flyOutBeeOne();
                    }
                }.start();
            }
        });
        animatorSet.start();
    }

    private void visitFlowerBeeTwo(HexagonImageView randomReadyFlower) {
        beeTwoTimer.cancel();
        gameAnimatorBeeTwo.getBeeAnimator().cancel();
        flyingBees.remove(beeTwoView);
        if (gameAnimatorBeeTwo.getBeeAnimator().isRunning()) return;

        int[] randomReadyFlowerPosition = new int[2];
        randomReadyFlower.getLocationOnScreen(randomReadyFlowerPosition);
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeTwoView, "translationX", randomReadyFlowerPosition[0] + beeOffsetX);
        Animator yAnimator = ObjectAnimator.ofFloat(beeTwoView, "translationY", randomReadyFlowerPosition[1] - beeOffsetY);
        xAnimator.setInterpolator(new DecelerateInterpolator(1f));
        yAnimator.setInterpolator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_LAND_ON_FLOWER);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                beeTwoView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.glowing_bee));
                new CountDownTimer(Constants.TIME_TO_STAY_ON_FLOWER, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) { }

                    @Override
                    public void onFinish() {
                        flowerUseCase.onHarvestingFlowerFinished(randomReadyFlower);
                        flyOutBeeTwo();
                    }
                }.start();
            }
        });
        animatorSet.start();
    }

    private void visitFlowerBeeThree(HexagonImageView randomReadyFlower) {
        beeThreeTimer.cancel();
        gameAnimatorBeeThree.getBeeAnimator().cancel();
        flyingBees.remove(beeThreeView);
        if (gameAnimatorBeeThree.getBeeAnimator().isRunning()) return;

        int[] randomReadyFlowerPosition = new int[2];
        randomReadyFlower.getLocationOnScreen(randomReadyFlowerPosition);
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeThreeView, "translationX", randomReadyFlowerPosition[0] + beeOffsetX);
        Animator yAnimator = ObjectAnimator.ofFloat(beeThreeView, "translationY", randomReadyFlowerPosition[1] - beeOffsetY);
        xAnimator.setInterpolator(new DecelerateInterpolator(1f));
        yAnimator.setInterpolator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_LAND_ON_FLOWER);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                beeThreeView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.glowing_bee));
                new CountDownTimer(Constants.TIME_TO_STAY_ON_FLOWER, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) { }

                    @Override
                    public void onFinish() {
                        flowerUseCase.onHarvestingFlowerFinished(randomReadyFlower);
                        flyOutBeeThree();
                    }
                }.start();
            }
        });
        animatorSet.start();
    }

    private void visitFlowerBeeFour(HexagonImageView randomReadyFlower) {
        beeFourTimer.cancel();
        gameAnimatorBeeFour.getBeeAnimator().cancel();
        flyingBees.remove(beeFourView);
        if (gameAnimatorBeeFour.getBeeAnimator().isRunning()) return;

        int[] randomReadyFlowerPosition = new int[2];
        randomReadyFlower.getLocationOnScreen(randomReadyFlowerPosition);
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeFourView, "translationX", randomReadyFlowerPosition[0] + beeOffsetX);
        Animator yAnimator = ObjectAnimator.ofFloat(beeFourView, "translationY", randomReadyFlowerPosition[1] - beeOffsetY);
        xAnimator.setInterpolator(new DecelerateInterpolator(1f));
        yAnimator.setInterpolator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_LAND_ON_FLOWER);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                beeFourView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.glowing_bee));
                new CountDownTimer(Constants.TIME_TO_STAY_ON_FLOWER, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) { }

                    @Override
                    public void onFinish() {
                        flowerUseCase.onHarvestingFlowerFinished(randomReadyFlower);
                        flyOutBeeFour();
                    }
                }.start();
            }
        });
        animatorSet.start();
    }

    private void visitFlowerBeeFive(HexagonImageView randomReadyFlower) {
        beeFiveTimer.cancel();
        gameAnimatorBeeFive.getBeeAnimator().cancel();
        flyingBees.remove(beeFiveView);
        if (gameAnimatorBeeFive.getBeeAnimator().isRunning()) return;

        int[] randomReadyFlowerPosition = new int[2];
        randomReadyFlower.getLocationOnScreen(randomReadyFlowerPosition);
        AnimatorSet animatorSet = new AnimatorSet();
        Animator xAnimator = ObjectAnimator.ofFloat(beeFiveView, "translationX", randomReadyFlowerPosition[0] + beeOffsetX);
        Animator yAnimator = ObjectAnimator.ofFloat(beeFiveView, "translationY", randomReadyFlowerPosition[1] - beeOffsetY);
        xAnimator.setInterpolator(new DecelerateInterpolator(1f));
        yAnimator.setInterpolator(new AccelerateInterpolator(1f));
        animatorSet.playTogether(xAnimator, yAnimator);
        animatorSet.setDuration(Constants.TIME_TO_LAND_ON_FLOWER);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                beeFiveView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.glowing_bee));
                new CountDownTimer(Constants.TIME_TO_STAY_ON_FLOWER, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) { }

                    @Override
                    public void onFinish() {
                        flowerUseCase.onHarvestingFlowerFinished(randomReadyFlower);
                        flyOutBeeFive();
                    }
                }.start();
            }
        });
        animatorSet.start();
    }

}
