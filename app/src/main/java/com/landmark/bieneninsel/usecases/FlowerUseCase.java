package com.landmark.bieneninsel.usecases;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.core.view.ViewCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.landmark.bieneninsel.R;
import com.landmark.bieneninsel.data.Constants;
import com.landmark.bieneninsel.data.PreferenceStoreManager;
import com.landmark.bieneninsel.events.WateringInProgressListener;
import com.landmark.bieneninsel.model.LevelOne;
import com.landmark.bieneninsel.model.PairID;
import com.landmark.bieneninsel.model.PlantCell;
import com.landmark.bieneninsel.ui.HexagonImageView;
import com.landmark.bieneninsel.ui.listener.HexFieldTouchListener;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.Month;
import org.threeten.bp.MonthDay;
import org.threeten.bp.Year;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java8.util.stream.StreamSupport;

public class FlowerUseCase {
    private Context context;
    private PreferenceStoreManager preferenceStoreManager;
    private LevelOne levelOneData;
    private List<View> hexFieldViews;
    private Map<String, LottieAnimationView> hexFieldViewAnimations;
    private View throwWaterAnimationView;
    private ProgressBar throwWaterProgressBarView;
    private ValueAnimator throwWaterStartAnimator;
    private ValueAnimator throwWaterStopAnimator;
    private WateringInProgressListener wateringInProgressListener;
    private HexFieldTouchListener hexFieldTouchListener;
    private List<View> readyFlowers; // happy and marked
    private CurrenciesUseCase currenciesUseCase;
    private CountDownTimer throwWaterProgressBarTimer;

    public FlowerUseCase(Context context, PreferenceStoreManager preferenceStoreManager, CurrenciesUseCase currenciesUseCase) {
        this.context = context;
        this.preferenceStoreManager = preferenceStoreManager;
        this.readyFlowers = new ArrayList<>();
        this.currenciesUseCase = currenciesUseCase;
    }

    public void setWateringInProgressListener(WateringInProgressListener listener) {
        wateringInProgressListener = listener;
    }

    public void setLevelOneData(LevelOne levelOneData) {
        this.levelOneData = levelOneData;
    }

    public void setHexFieldViews(List<View> hexFieldViews) {
        this.hexFieldViews = hexFieldViews;
    }

    public void setHexFieldTouchListener(HexFieldTouchListener hexFieldTouchListener) {
        this.hexFieldTouchListener = hexFieldTouchListener;
    }

    public void setHexFieldViewAnimations(Map<String, LottieAnimationView> hexFieldViewAnimations) {
        this.hexFieldViewAnimations = hexFieldViewAnimations;
    }

    // =============================== Used by Activity ===============================

    private View getHexFieldView(PlantCell plantCell) {
        return StreamSupport
                .stream(hexFieldViews)
                .filter(view -> ((PairID) view.getTag()).equals(plantCell.getId()))
                .findAny()
                .orElse(null);
    }

    /* Restores/updates the game board, all Level 1 hex-fields / flowers.
       Locks board to impose one flower/day rule.
       */
    public void restoreHoneycomb() {
        for (PlantCell plantCell : levelOneData.getPlantCells()) {
            View hexFieldView = getHexFieldView(plantCell);
            PlantCell savedCell = preferenceStoreManager.getHoneycombData(plantCell.getId().toString());
            if (savedCell != null) {
                updateHexFields(savedCell, plantCell, hexFieldView);
                if (savedCell.isMarked() && !savedCell.isBusy() && !readyFlowers.contains(hexFieldView)) readyFlowers.add(hexFieldView);
            }
        }
        if (preferenceStoreManager.getLockingCell() != null) {
            lockLevelOne((HexagonImageView) getHexFieldView(preferenceStoreManager.getLockingCell()));
        }
    }

    // Updates a single hex-field / flower
    public void updateHexField(PairID flowerId) {
        PlantCell savedCell = preferenceStoreManager.getHoneycombData(flowerId.toString());
        View hexFieldView = getHexFieldView(savedCell);
        if (savedCell != null) {
            updateHexFields(savedCell, savedCell, hexFieldView);
        }
    }

   /* FLOWER STATE MACHINE - Handles use-cases to make soil, grow plant, grow flower and change flower state between happy and sad */
    private void updateHexFields(PlantCell savedCell, PlantCell plantCell, View hexFieldView) {
        // TODO: return days
//                if (savedCell.getDayToGrowFlower() != null) {
//                    if (today.isEqual(savedCell.getDayToGrowFlower())) { // happy flower
//                        animateFlower(flowerView, 0.4f, 0.6f);
//                    } else { // plant
//                        animateFlower(flowerView, 0.2f, 0.4f);
//                    }
//                }

        LottieAnimationView flowerView = hexFieldViewAnimations.get(hexFieldView.getTag().toString());
        if (flowerView != null) {
            flowerView.setVisibility(View.VISIBLE);
            if (savedCell.isWatered()) { // WATERED
                // FLOURISHED (sad or happy) -> Happy flower
                if (savedCell.isFlourished()) {
                    if (savedCell.getLastTimeWatered() != null) {
                        if (LocalDateTime.now().compareTo(savedCell.getLastTimeWatered()) > 0 && LocalDateTime.now().compareTo(savedCell.getDayToCheckFlower()) >= 0) {
                            animateFlower(flowerView, .6f, .6f); // happy flower
                            plantCell.setHappy(true);
                            setNextTimeToCheckFlower(plantCell);
                            if (savedCell.isMarked()) {
                                animateFlower(flowerView, .6f, .8f); // glowing mark flower
                            }
                        } else {
                            if (savedCell.getTimeToUnMarkFlower() != null &&  LocalDateTime.now().compareTo(savedCell.getTimeToUnMarkFlower()) > 0 ) {
                                plantCell.setPollinated(false);
                                plantCell.setTimeToUnMarkFlower(null);
                                plantCell.setMarked(false);
                                removeReadyFlower((HexagonImageView) hexFieldView);
                                preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
                            }
                            // Restore watered flourished flower state when game comes to foreground. Do not restart timers to check flower state
                            if (savedCell.isHappy()) {
                                if (savedCell.isMarked()) {
                                    animateFlower(flowerView, .6f, .8f); // glowing mark flower
                                } else {
                                    animateFlower(flowerView, .6f, .6f); // happy flower
                                }
                            } else {
                                animateFlower(flowerView, 1f, 1f); // sad flower
                            }
                        }
                    }
                }
                // SOIL -> Happy flower (flourished) or Plant
                if (savedCell.isSoiled() && savedCell.getDayToGrowFlower() != null) {
                    if (LocalDateTime.now().toLocalDate().isEqual(savedCell.getDayToGrowFlower().toLocalDate())) {
                        animateFlower(flowerView, .4f, .6f); // happy flower
                        plantCell.setHappy(true);
                        setNextTimeToCheckFlower(plantCell);
                        unLockLevelOne();
                    } else {
                        animateFlower(flowerView, .4f, .4f); // plant
                    }
                }
            } else { // NON-WATERED
                // SOIL
                if (savedCell.isSoiled()) {
                    animateFlower(flowerView, 0f, 0f); // soil
                }
                // FLOURISHED (sad or happy) -> Sad flower
                if (savedCell.isFlourished()) {
                    if (savedCell.getDayToCheckFlower() != null && LocalDateTime.now().compareTo(savedCell.getDayToCheckFlower()) >= 0) {
                        animateFlower(flowerView, 1f, 1f); // sad flower
                        plantCell.setHappy(false);
                        plantCell.setMarked(false);
                        removeReadyFlower((HexagonImageView) hexFieldView);
                        setNextTimeToCheckFlower(plantCell);
                    } else {
                        if (savedCell.getTimeToUnMarkFlower() != null && LocalDateTime.now().compareTo(savedCell.getTimeToUnMarkFlower()) > 0 ) {
                            plantCell.setPollinated(false);
                            plantCell.setTimeToUnMarkFlower(null);
                            plantCell.setMarked(false);
                            removeReadyFlower((HexagonImageView) hexFieldView);
                            preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
                        }
                        // restore flourished flower state when opening game. Do not restart timers to check flower state
                        if (savedCell.isHappy()) {
                            if (savedCell.isMarked()) {
                                animateFlower(flowerView, .6f, .8f); // glowing mark flower
                            } else {
                                animateFlower(flowerView, .6f, .6f); // happy flower
                            }
                        } else {
                            animateFlower(flowerView, 1f, 1f); // sad flower
                        }
                    }
                }
            }
        }
    }

    // make hex-fields non clickable so that user does not plant a new flower in same day
    // just for first flowers of hex-fields = plant -> flower transition
    // do not lock flourished flowers, because they need water each day
    public void lockLevelOne(HexagonImageView soiledHexField) {
        for (PlantCell plantCell : levelOneData.getPlantCells()) {
            View hexFieldView = getHexFieldView(plantCell);
            if (hexFieldView.getTag() == soiledHexField.getTag()) continue;
            PlantCell currentPlantCell = getPlantCell((HexagonImageView) hexFieldView);
            if (currentPlantCell != null) {
                if (currentPlantCell.getColor() == R.color.groundDark) {
                    if (!currentPlantCell.isFlourished()) hexFieldView.setOnTouchListener(null);
                }
            }
        }
        preferenceStoreManager.setLockingCell(getPlantCell(soiledHexField));
    }

    // make hex-fields clickable again
    private void unLockLevelOne() {
        for (PlantCell plantCell : levelOneData.getPlantCells()) {
            View hexFieldView = getHexFieldView(plantCell);
            if (plantCell != null) {
                if (plantCell.getColor() == R.color.groundDark) {
                    hexFieldView.setOnTouchListener(hexFieldTouchListener);
                }
            }
        }
        preferenceStoreManager.setLockingCell(null);
    }

    // make soiled hex-field clickable again after watering canceled
    private void unLockSoiledCell(HexagonImageView soiledHexField) {
        soiledHexField.setOnTouchListener(hexFieldTouchListener);
    }

    // resets a flower state after being happy or sad and registers next alarm to update the flower
    private void setNextTimeToCheckFlower(PlantCell plantCell) {
        plantCell.setWatered(false);
        plantCell.setLastTimeWatered(null);
        plantCell.setDayToGrowFlower(null);
        plantCell.setSoiled(false);
        LocalDateTime dayToCheckFlower = LocalDateTime.now().plusDays(1);
        LocalDateTime dayTimeToCheckFlower = LocalDateTime.of(dayToCheckFlower.getYear(), dayToCheckFlower.getMonth(), dayToCheckFlower.getDayOfMonth(), 00, 01);
//        LocalDateTime timeToCheckFlower = LocalDateTime.now().plusMinutes(5); // TODO: delete for release, check every 5 mins for demo
        plantCell.setDayToCheckFlower(dayTimeToCheckFlower);
        plantCell.setFlourished(true);
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
        Intent alarmIntent = new Intent("ALARM_TO_CHECK_FLOWER");
        alarmIntent.putExtra("TIME_TO_CHECK_FLOWER", dayTimeToCheckFlower);
        alarmIntent.putExtra("FLOWER_TO_CHECK", plantCell.getId());
        context.sendBroadcast(alarmIntent);
    }

    private void animateFlower(View flowerView, float startAnimation, float endAnimation) {
        ValueAnimator animator = ValueAnimator.ofFloat(startAnimation, endAnimation);
        animator.addUpdateListener(animation -> ((LottieAnimationView) flowerView).setProgress(Float.valueOf(animation.getAnimatedValue().toString())));
        animator.setDuration(Constants.FLOWER_ANIMATION_DURATION);
        animator.start();
    }

    // =============================== Used by Gesture Listener ===============================

    // Get latest Plant info by querying data in shared pref memory first.
    // If there is no plant data in shared pref memory then it gets it from local memory.
    public PlantCell getPlantCell(HexagonImageView view) {
        PlantCell savedCell = preferenceStoreManager.getHoneycombData(view.getTag().toString());
        if (savedCell != null) return savedCell;
        else {
            return StreamSupport
                    .stream(levelOneData.getPlantCells())
                    .filter(cell -> cell.getId().first == view.getHex().getQ() && cell.getId().second == view.getHex().getR())
                    .findAny()
                    .orElse(null);
        }
    }

    public void createSoil(HexagonImageView hexagonImageView, PlantCell plantCell) {
        LottieAnimationView animationView = hexFieldViewAnimations.get(hexagonImageView.getTag().toString());
        if (animationView != null) {
            animationView.setVisibility(View.VISIBLE);
            animateCreateSoil(animationView);
            updateSoiledPlantCell(plantCell);
        }
    }

    // Limit one water can per flower at a time. Release water can at the end of progress bar.
    public void startThrowWater(HexagonImageView view, PlantCell plantCell) {
        if (throwWaterAnimationView == null) {
            throwWaterAnimationView = inflateAnimationToHexField(view, R.layout.water_animation_view);
            ViewCompat.setTranslationZ(throwWaterAnimationView, 1);
        }
        if (throwWaterProgressBarView == null) throwWaterProgressBarView = (ProgressBar) inflateAnimationToHexField(view, R.layout.watering_progress_bar);
        if (throwWaterAnimationView != null && throwWaterProgressBarView != null) {
            animateStartThrowWater(0f, .6f, view, plantCell); // start throw water
            throwWaterProgressBar(view, plantCell);
        }
    }

    private View inflateAnimationToHexField(HexagonImageView view, int animationId) {
        ViewGroup parent = (ViewGroup) view.getParent();
        if (animationId != 0) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View animationView = inflater.inflate(animationId, parent, false);
            parent.addView(animationView, view.getLayoutParams());
            return animationView;
        }
        return null;
    }

    private void deflateAnimationFromHexField(HexagonImageView hexagonImageView) {
        ViewGroup parent = (ViewGroup) hexagonImageView.getParent();
        View waterAnimationView = ((Activity) context).findViewById(R.id.waterAnimationView);
        parent.removeView(waterAnimationView);
    }

    private void animateCreateSoil(View animationView) {
        ValueAnimator animator = ValueAnimator.ofFloat(0f, 0f); // create soil
        animator.addUpdateListener(animation -> ((LottieAnimationView) animationView).setProgress(Float.valueOf(animation.getAnimatedValue().toString())));
        animator.start();
    }

    private void animateStartThrowWater(float startAnimation, float endAnimation, HexagonImageView view, PlantCell plantCell) {
        throwWaterStartAnimator = ValueAnimator.ofFloat(startAnimation, endAnimation);
        throwWaterStartAnimator.addUpdateListener(animation -> ((LottieAnimationView) throwWaterAnimationView).setProgress(Float.valueOf(animation.getAnimatedValue().toString())));
        throwWaterStartAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) { }

            @Override
            public void onAnimationCancel(Animator animation) {
                animateStopThrowWater(0.85f, 1f, view, plantCell, false); // stop throw water
            }

            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
        throwWaterStartAnimator.start();
    }

    private void animateStopThrowWater(float startAnimation, float endAnimation, HexagonImageView view, PlantCell plantCell, Boolean cancelOperation) {
        throwWaterStopAnimator = ValueAnimator.ofFloat(startAnimation, endAnimation);
        throwWaterStopAnimator.addUpdateListener(animation -> ((LottieAnimationView) throwWaterAnimationView).setProgress(Float.valueOf(animation.getAnimatedValue().toString())));
        throwWaterStopAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (cancelOperation) {
                    throwWaterProgressBarTimer.cancel();
                }
                unLockSoiledCell(view);
                if (throwWaterProgressBarView != null) throwWaterProgressBarView.setVisibility(View.GONE);
                throwWaterProgressBarView = null;
                throwWaterAnimationView = null;
                if (wateringInProgressListener != null) wateringInProgressListener.isWatering(false);
                throwWaterStopAnimator.removeAllUpdateListeners();
                throwWaterStartAnimator.removeAllUpdateListeners();
                deflateAnimationFromHexField(view);
                if (!cancelOperation) {
                    PlantCell tempPlantCell = updateWateredPlantCell(plantCell);
                    if (plantCell.isSoiled())
                        setFlowerGrowthRules(tempPlantCell); // set the target date to grow flower for first time
                    LottieAnimationView flowerView = hexFieldViewAnimations.get(view.getTag().toString());
                    if (flowerView != null) {
                        flowerView.setVisibility(View.VISIBLE);
                        if (plantCell.isSoiled()) animateFlower(flowerView, 0f, .4f); // plant
                    }
                    if (plantCell.isFlourished() || plantCell.isSoiled()) currenciesUseCase.incrementPflegepunkte();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) { }

            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
        throwWaterStopAnimator.start();
    }

    private void throwWaterProgressBar(HexagonImageView view, PlantCell plantCell) {
        if (wateringInProgressListener != null) wateringInProgressListener.isWatering(true);
        throwWaterProgressBarView.setVisibility(View.VISIBLE);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        return true;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        animateStopThrowWater(0.85f, 1f, view, plantCell, true); // stop throw water
                        return false;
                }
                return true;
            }
        });
        throwWaterProgressBarTimer = new CountDownTimer(Constants.TIME_TO_WATER, 1000) {
            Long goneMilliSeconds;

            @Override
            public void onTick(long millisUntilFinished) {
                goneMilliSeconds = Constants.TIME_TO_WATER - millisUntilFinished;
                int goneSeconds = goneMilliSeconds.intValue() / 1000;
                int progressStatus = (goneSeconds * 10) / 2;
                throwWaterProgressBarView.setProgress(progressStatus);
            }

            @Override
            public void onFinish() {
                view.setOnTouchListener(null);
                animateStopThrowWater(0.85f, 1f, view, plantCell, false); // stop throw water
            }

        }.start();
    }

    // put cells/hex-fields on groups after user taps on a hex-field first time.
    public void setHexFieldGroup(PlantCell plantCell) {
        int levelOneFirstGroupCounter = preferenceStoreManager.getLevelOneFirstGroupCounter();
        int levelOneSecondGroupCounter = preferenceStoreManager.getLevelOneSecondGroupCounter();
        int levelOneThirdGroupCounter = preferenceStoreManager.getLevelOneThirdGroupCounter();

        if (levelOneFirstGroupCounter < Constants.LEVEL_ONE_FIRST_GROUP_COUNTER) {
            preferenceStoreManager.setLevelOneFirstGroupCounter(levelOneFirstGroupCounter + 1);
            plantCell.setGroupId(Constants.GROUP_ONE);
        } else if (levelOneSecondGroupCounter < Constants.LEVEL_ONE_SECOND_GROUP_COUNTER) {
            preferenceStoreManager.setLevelOneSecondGroupCounter(levelOneSecondGroupCounter + 1);
            plantCell.setGroupId(Constants.GROUP_TWO);
        } else {
            preferenceStoreManager.setLevelOneThirdGroupCounter(levelOneThirdGroupCounter + 1);
            plantCell.setGroupId(Constants.GROUP_THREE);
        }
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
    }

    // set the rule for growing flower based on group id after watering the cell/hex-field
    private void setFlowerGrowthRules(PlantCell plantCell) {
        LocalDateTime dayToGrowFlower = null;
        LocalDateTime groupOneDayToGrow = LocalDateTime.now().plusDays(Constants.GROUP_ONE);
        LocalDateTime groupTwoDayToGrow = LocalDateTime.now().plusDays(Constants.GROUP_TWO);
        LocalDateTime groupThreeDayToGrow = LocalDateTime.now().plusDays(Constants.GROUP_THREE);

        if (plantCell.isSoiled() && plantCell.isWatered()) {
            if (plantCell.getGroupId() == Constants.GROUP_ONE) {
                dayToGrowFlower = LocalDateTime.of(groupOneDayToGrow.getYear(), groupOneDayToGrow.getMonth(), groupOneDayToGrow.getDayOfMonth(), 00, 01);
                plantCell.setDayToGrowFlower(dayToGrowFlower); // grow happy flower next day
            }
            if (plantCell.getGroupId() == Constants.GROUP_TWO) {
                dayToGrowFlower = LocalDateTime.of(groupTwoDayToGrow.getYear(), groupTwoDayToGrow.getMonth(), groupTwoDayToGrow.getDayOfMonth(), 00, 01);
                plantCell.setDayToGrowFlower(dayToGrowFlower); // grow plant next day, happy flower after 2 days
            }
            if (plantCell.getGroupId() == Constants.GROUP_THREE) {
                dayToGrowFlower = LocalDateTime.of(groupThreeDayToGrow.getYear(), groupThreeDayToGrow.getMonth(), groupThreeDayToGrow.getDayOfMonth(), 00, 01);
                plantCell.setDayToGrowFlower(dayToGrowFlower); // grow plant next day, happy flower after 3 days
            }
        }
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);

        Intent alarmIntent = new Intent("ALARM_TO_GROW_FLOWER");
        alarmIntent.putExtra("TIME_TO_GROW_FLOWER", dayToGrowFlower);
        alarmIntent.putExtra("FLOWER_TO_GROW", plantCell.getId());
        context.sendBroadcast(alarmIntent);
    }

//    // TODO: demo, delete for release
//    private void setFlowerGrowthRules(PlantCell plantCell) {
//        LocalDateTime timeToGrowFlower = null;
//
//        if (plantCell.isSoiled() && plantCell.isWatered()) {
//            if (plantCell.getGroupId() == Constants.GROUP_ONE) {
//                timeToGrowFlower = LocalDateTime.now().plusMinutes(1);
//                plantCell.setTimeToGrowFlower(timeToGrowFlower);
//            }
//            if (plantCell.getGroupId() == Constants.GROUP_TWO) {
//                timeToGrowFlower = LocalDateTime.now().plusMinutes(2);
//                plantCell.setTimeToGrowFlower(timeToGrowFlower);
//            }
//            if (plantCell.getGroupId() == Constants.GROUP_THREE) {
//                timeToGrowFlower = LocalDateTime.now().plusMinutes(3);
//                plantCell.setTimeToGrowFlower(timeToGrowFlower);
//            }
//        }
//
//        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
//
//        Intent alarmIntent = new Intent("ALARM_TO_GROW_FLOWER");
//        alarmIntent.putExtra("TIME_TO_GROW_FLOWER", timeToGrowFlower);
//        alarmIntent.putExtra("FLOWER_TO_GROW", plantCell.getId());
//        context.sendBroadcast(alarmIntent);
//    }

    private void updateSoiledPlantCell(PlantCell plantCell) {
        plantCell.setSoiled(true);
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
    }

    private PlantCell updateWateredPlantCell(PlantCell plantCell) {
        plantCell.setWatered(true);
        plantCell.setLastTimeWatered(LocalDateTime.now());
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
        return plantCell;
    }

    // --------------------------  MARK / UNMARK FLOWER FOR BEE VISIT   ---------------------------- //

    public void markFlowerForHarvesting(HexagonImageView hexagonImageView, PlantCell plantCell) {
        updateMarkedFlower(plantCell);
        readyFlowers.add(hexagonImageView);
        updateHexFields(plantCell, plantCell, hexagonImageView);
    }

    private void updateMarkedFlower(PlantCell plantCell) {
        plantCell.setMarked(true);
        plantCell.setBusy(false);
        LocalDateTime timeToUnMarkFlower = LocalDateTime.now().plusHours(Constants.TIME_TO_UNMARK_FLOWER); // unmark flower after 2 hours from last mark
//        LocalDateTime timeToUnMarkFlower = LocalDateTime.now().plusMinutes(2); // TODO: delete for release, 2 mins for sake of demo
        plantCell.setTimeToUnMarkFlower(timeToUnMarkFlower);
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);

        Intent alarmIntent = new Intent("ALARM_TO_UNMARK_FLOWER");
        alarmIntent.putExtra("TIME_TO_UNMARK_FLOWER", timeToUnMarkFlower);
        alarmIntent.putExtra("FLOWER_TO_UNMARK", plantCell.getId());
        context.sendBroadcast(alarmIntent);
    }

    public void onHarvestingFlowerFinished(HexagonImageView hexagonImageView) {
        PlantCell plantCell = getPlantCell(hexagonImageView);
        if (plantCell.isMarked()) {
            readyFlowers.add(hexagonImageView); // add back ready flower to available flowers to be visited by flying bees
            setBusyFlower(hexagonImageView, false);
        }
        updateHarvestedFlower(plantCell);
        currenciesUseCase.incrementBlutenstaub();
    }

    private void updateHarvestedFlower(PlantCell plantCell) {
        plantCell.setPollinated(true);
        plantCell.setLastTimePollinated(LocalDateTime.now());
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
    }

    public List<View> getReadyFlowers() {
        return readyFlowers;
    }

    public void removeReadyFlower(HexagonImageView hexagonImageView) {
        readyFlowers.remove(hexagonImageView);
    }

    public void setBusyFlower(HexagonImageView hexagonImageView, boolean busy) {
        PlantCell plantCell = getPlantCell(hexagonImageView);
        plantCell.setBusy(busy);
        preferenceStoreManager.setHoneycombData(plantCell.getId().toString(), plantCell);
    }

}
