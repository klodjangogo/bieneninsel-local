package com.landmark.bieneninsel.usecases;

import android.content.Context;

import com.landmark.bieneninsel.data.PreferenceStoreManager;
import com.landmark.bieneninsel.ui.curencies.CurrencyView;

public class CurrenciesUseCase {
    final String BLUTENSTAUB = "Blutenstaub";
    final String PFLEGEPUNKTE = "Pflegepunkte";

    private Context context;
    private PreferenceStoreManager preferenceStoreManager;
    private CurrencyView blutenstaubView;
    private CurrencyView pflegepunkteView;

    public CurrenciesUseCase(Context context, PreferenceStoreManager preferenceStoreManager, CurrencyView blutenstaubView, CurrencyView pflegepunkteView) {
        this.context = context;
        this.preferenceStoreManager = preferenceStoreManager;
        this.blutenstaubView = blutenstaubView;
        this.pflegepunkteView = pflegepunkteView;
        init();
    }

    private void init() {
        blutenstaubView.setText(String.valueOf(preferenceStoreManager.getPoint(BLUTENSTAUB)));
        pflegepunkteView.setText(String.valueOf(preferenceStoreManager.getPoint(PFLEGEPUNKTE)));
    }

    public void incrementBlutenstaub() {
        int blutenstaub = preferenceStoreManager.getPoint(BLUTENSTAUB) + 1;
        blutenstaubView.setText(String.valueOf(blutenstaub));
        preferenceStoreManager.setPoint(BLUTENSTAUB, blutenstaub);
    }

    public void incrementPflegepunkte() {
        int pflegepunkte = preferenceStoreManager.getPoint(PFLEGEPUNKTE) + 1;
        pflegepunkteView.setText(String.valueOf(pflegepunkte));
        preferenceStoreManager.setPoint(PFLEGEPUNKTE, pflegepunkte);
    }

}
